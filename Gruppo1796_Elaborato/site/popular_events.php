<div>
    <script src="cart_btn.js"></script>
    <?php
        require_once("connection.php");
        $carousel_dim;
        $popular_events = array();


        
        if($stmt = $conn->prepare("SELECT * FROM `evento` ORDER BY Biglietti_venduti DESC LIMIT 5")){
            //Esegui
            $stmt->execute();

            //Ottieni result
            $events = $stmt->get_result();
            
            //Chiudi statement
            $stmt->close();
        }else {  
            //Se avviene un errore, stampo il codice e stampo link per redirect
            $error = $conn->errno . ' ' . $conn->error;
            echo $error;
            echo "<p>Errore nello script, torna alla pagina di creazione</p>";
            echo "<a href='insert_manifest.php'>Torna</p>";
        }
        
        if($events->num_rows > 0){

            echo "<div class='card-title'>
            <h2>Eventi Popolari</h2></div>";

            for($i=0;$i<$events->num_rows;$i++){
                $popular_events[$i] = $events->fetch_assoc();
            }
            $carousel_dim = sizeof($popular_events);
            
            //Echo a carousel with the data acquired
            //Indicators:
            echo "<div id='popularCarousel' class='carousel slide' data-ride='carousel'>
            <div class='carousel-inner'>";
                    
            for($i=0; $i < $carousel_dim; $i++){
                $name = $popular_events[$i]['Nome_evento'];
                $desc = $popular_events[$i]['Descrizione'];
                $event_id = $popular_events[$i]['Codice_evento'];
                $image = $popular_events[$i]["Img_evento"];      
                $stmt = $conn->prepare("SELECT Importo FROM `prodotto` WHERE FK_Codice_evento=? ORDER BY Importo asc LIMIT 1");
                $stmt->bind_param('i', $event_id);
                $stmt->execute();
                $result = $stmt->get_result();
                $stmt->close();
                $price=NULL;
                if (isset($result)) {
                    $ticket = $result->fetch_assoc();
                    $price=$ticket["Importo"];
                } else {
                    $price = "err";
                }
                if($i==0){//The first Carousel item, must be set active
                    echo"<div class='carousel-item active' data-ride='carousel'>
                        <a href='event-page.php?event=$event_id' class='card-link'>
                            <img class='carousel-img card-img-top img-fluid' src='$image' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>    
                        </a>
                        <div class='card-body text-center'>
                            <h4 class='card-title'> $name </h4>
                            <p class='card-text'> $desc </p>
                            <a href='event-page.php?event=$event_id' role='button' class='card-link'>
                                <button type='button' class='btn btn-secondary'>A partire da $price $</button>
                                </a>
                        </div>
                    </div>";
                } else {
                    echo"<div class='carousel-item' data-ride='carousel'>
                        <a href='event-page.php?event=$event_id' class='card-link'>
                            <img class='carousel-img card-img-top img-fluid' src='$image' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>    
                        </a>    
                        <div class='card-body text-center'>
                            <h4 class='card-title'> $name </h4>
                            <p class='card-text'> $desc </p>
                            <a href='event-page.php?event=$event_id' class='card-link'>
                                <button type='button' class='btn btn-secondary'>A partire da $price $</button>
                            </a>
                        </div>
                    </div>";
                }
                
            }

            //Carousel navigation arrows
            echo "</div>            
                    <a class='carousel-control-prev' href='#popularCarousel' role='button' data-slide='prev'>
                        <span class='carousel-control-prev-icon' aria-hidden='true'></span>
                        <span class='sr-only'>Previous</span>
                    </a>
                                
                    <a class='carousel-control-next' href='#popularCarousel' role='button' data-slide='next'>
                        <span class='carousel-control-next-icon' aria-hidden='true'></span>
                        <span class='sr-only'>Next</span>
                    </a>
                </div>";
        }
    ?>
</div>