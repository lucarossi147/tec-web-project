<?php 
    class User{
        
        //Proprietà
        public $name="";
        private $events;
        
        //Costruttore
        public function __construct($guest_name){
            $this->name=$guest_name;
            $this->events=array();
            fillEvents();
        }

        //Ritorna lista di eventi
        public function getEvents(){
            return $this->events;
        }
        
        //Aggiungi evento
        public function addEvent($event_code){
            array_push($events,$event_code);
        }

        private function fillEvents(){
            // Create connection
            require_once("connection.php");

            $sql = "SELECT FK_Codice_evento FROM `partecipazione` WHERE FK_Nome_ospite='$this->name'";
            $events = $conn->query($sql);
            if($events->num_rows>0){
                while($event_code = $events->fetch_assoc()){
                    addEvent($event_code);
                }
            }
        }
    }
?>