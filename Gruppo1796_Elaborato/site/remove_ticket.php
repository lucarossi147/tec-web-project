<?php
    require_once("connections.php");

    // sql to delete a record
    $sql = "DELETE FROM `ricevuta` WHERE `Data_acquisto` <= DATE_SUB(CURDATE(), INTERVAL 10 YEAR)";

    if ($conn->query($sql) === TRUE) {
        echo "Record eliminato";
    } else {
        echo "Errore: " . $conn->error;
    }

    $conn->close();
?>