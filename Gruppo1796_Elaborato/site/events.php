<?php
  include 'functions.php';
  sec_session_start();?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
 
    <!-- Main Css -->
     <link rel="stylesheet" href="css/main.css">
    <title>Home | Event.ino</title>
  </head>

  <body>

    <!-- Header -->
    <?php
      require_once("header.php");
    ?>
 
    <!-- Retrieve data from db -->
    <?php
        require_once("connection.php");

    //Sql statement
    $sql = "SELECT * FROM `evento`";
    $result = $conn->query($sql);

    if ($result->num_rows > 0) {
        // output data of each row, multiple row
        while($row = $result->fetch_assoc()) {
          $nome_evento=$row["Nome_evento"];
          $img=$row["Img_evento"];
          $desc=$row["Descrizione"];
          $num_event=$row["Codice_evento"];
          $sql2 = "SELECT Importo FROM `prodotto` WHERE FK_Codice_evento=$num_event 
          ORDER BY Importo asc LIMIT 1";
          $result2 = $conn->query($sql2);
          if (isset($result2)) {
            $ticket = $result2->fetch_assoc();
            $price=$ticket["Importo"];
          } else {
            $price = "err";
          }
          echo "<div class='card'>
                  <a href='event-page.php?event=$num_event' class='card-link'>
                    <img class='carousel-img card-img-top img-fluid' src='$img' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>
                  </a>
                  <div class='card-body'>
                    <h5 class='card-title'> $nome_evento </h5>
                    <p class='card-text'> $desc </p>
                    <a href='event-page.php?event=$num_event' class='card-link'>
                      <button type='button' class='btn btn-secondary'>A partire da $price $</button>
                    </a>
                  </div>
                </div>";
           
          }
            
        } else {
        echo "0 results";
    }

    $conn->close();
    ?>


    <!-- Footer -->
    <?php
      require_once("footer.html");
    ?>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>

</html>