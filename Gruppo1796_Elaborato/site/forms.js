function formhash(form, password) {
    // Crea un elemento di input che verrà usato come campo di output per la password criptata.
    var p = document.createElement("input");
    // Aggiungi un nuovo elemento al tuo form.
    form.appendChild(p);
    p.name = "p";
    p.type = "hidden"
    p.value = hex_sha512(password.value);
    // Assicurati che la password non venga inviata in chiaro.
    password.value = "";
    // Come ultimo passaggio, esegui il 'submit' del form.
    form.submit();
 }

 function formhashsignup(form, password, passconf) {
   p1 = password.value;
   p2 = passconf.value;
   var n = p1.localeCompare(p2);
   if(n == 0){
      console.log("bono");
      // Crea un elemento di input che verrà usato come campo di output per la password criptata.
      var p = document.createElement("input");
      // Aggiungi un nuovo elemento al tuo form.
      form.appendChild(p);
      p.name = "p";
      p.type = "hidden"
      p.value = hex_sha512(password.value);
      // Assicurati che la password non venga inviata in chiaro.
      password.value = "";
      passconf.value = "";
      // Come ultimo passaggio, esegui il 'submit' del form.
      form.submit();
    }else {
      var err = document.createElement("P");
      form.appendChild(err);
      err.value="le password sono diverse, prova a reinserirle";
      password.value = "";
      passconf.value = "";
    }
 }