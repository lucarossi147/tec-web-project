<?php
include 'functions.php';
sec_session_start();
$server="localhost";
$usr="root";
$pwd="";
$db="eventino_db";

if (isset($_POST)){
    session_unset();
    $mysqli = new mysqli($server,$usr,$pwd,$db);

    if ($mysqli -> connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
        exit();
    }

    $input = htmlentities($_POST['email'], ENT_QUOTES);
    $pass = htmlentities($_POST['p'], ENT_QUOTES);

    if (login($input, $pass, $mysqli)==true){
        $_SESSION["success"]="Loggato con successo";
        //echo('yes');
        header("Location:index.php");
        exit;
    }else{
        if(!isset($_SESSION["disabilitato"])){
            $_SESSION["error"]="Username o password errati";
        }
       // echo('ops');
        header("Location:login.php");
        exit;
    }
    $mysqli->close();
}
?>