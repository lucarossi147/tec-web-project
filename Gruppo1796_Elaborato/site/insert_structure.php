<?php
  include 'functions.php';
  sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Jquery Libraries -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

    <!-- Ajax lib 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    -->
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
    crossorigin="anonymous">

    <!-- Jquery-Ui -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Main Css -->
    <link rel="stylesheet" href="css/main.css">

    <script src="control.js"></script>
    <script src="verify.js"></script>

    <title> Insert strucuture | Event.ino </title>
  </head>

  <body>
    <?php
        //Db connection
        require_once("connection.php");

        // Header    
        require_once("header.php");   
        ?>
        <form class="needs-validation" novalidate>
        <div>
          <!-- Selezione tipologia evento --> 
          <div class="col">
                <label for="form-selected-regione" >Regione</label>
                <select class="form-control" id="form-selected-regione" name="form-selected-regione"> 
                <?php
                    $sql = "SELECT DISTINCT `Regione` FROM `localita_` WHERE 1 ORDER BY `localita_`.`Regione` ASC";
                    $result = $conn->query($sql);    
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { echo '<option>' . $row['Regione'] . '</option>';}
                    } else {
                        echo "0 results";
                    } 
                ?>
                </select>
            </div>
            <div class="col">
                <label for="form-selected-provincia">Provincia</label>
                <select class="form-control" id="form-selected-provincia" name="form-selected-provincia"> 
                </select>
            </div>
            <div class="col">
                <label for="form-selected-localita">Località</label>
                <select class="form-control" id="form-selected-localita" name="form-selected-localita"> 
                </select>
            </div>
            
             <!--Insert nome-->
             <div class="col">
                <label for="struct-name">Nome struttura:</label>
                <input value ="" type="text" class="form-control" id="struct-name" placeholder="Nome struttura" autocomplete="off" 
                name="struct-name" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci il nome della struttura</div>
            </div>
            <!--Insert immagine-->
            <div class="col">
                <label for="struct-img">Percorso immagine:</label>
                <input type="text" class="form-control" id="struct-img" placeholder="Percorso immagine" name="struct-img" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci immagine</div>
            </div>
            <!--Insert indirizzo-->
            <div class="col">
                <label for="struct-indirizzo">Indirizzo:</label>
                <input type="text" class="form-control" id="struct-indirizzo" placeholder="Indirizzo" 
                name="struct-indirizzo" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci indirizzo</div>
            </div>
            <!--Insert Capienza-->
            <div class="col">
                <label for="struct-capienza"> Capienza: </label>
                <input type="number" class="form-control" id="struct-capienza" placeholder="0" 
                name="struct-capienza" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci numero ospiti</div>
            </div>

            <!--Insert Posti disponibili-->
            <div class="col">
                <label for="posti-disponibili"> Posti disponibili: </label>
                <input type="number" class="form-control" id="posti-disponibili" placeholder="0" 
                name="posti-disponibili" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci numero ospiti</div>
            </div>
            <label id="disable" for="disable_button"></label>
            <button id="disable_button" formaction='add_structure.php' 
                    formmethod='POST' type='submit' class='btn btn-primary mb-2'> Inserisci struttura </button>
        </div>
        </form>

        <?php
            echo "<a href='insert_event.php'>
            <button type='button' class='btn btn-primary'>NEXT</button>
          </a>";
        ?>
        <script> // Disable form submissions if there are invalid fields
            (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Get the forms we want to add validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
                });
            }, false);
            })();
</script>
    <?php  
        // Footer 
        require_once("footer.html");
    ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>

</html>