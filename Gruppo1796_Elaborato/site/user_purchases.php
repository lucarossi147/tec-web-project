<?php
    include 'functions.php';
    sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

        <!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
  
    <title>Acquisti</title>
  </head>

  <body>
    
    <?php
        require_once("header.php");
    ?>
    <h1>I Tuoi acquisti</h1>
    </br>
    <?php
      
      require_once("connection.php");

      if(login_check($conn)){
        $user = $_SESSION["username"];
        $sql = $conn->prepare("SELECT * FROM `acquisto` WHERE (Data_acquisto IS NOT NULL)
                                                        AND (FK_UserName=?)");
        $sql->bind_param('s', $user);
        $sql->execute();
        $acquisti = $sql->get_result();
        $sql->close();
        echo "<div class='container'>";

        if($acquisti->num_rows > 0){
            while($acquisto = $acquisti->fetch_assoc()){
                $codice_acquisto = $acquisto["Codice_acquisto"];
                $sql_prodotto = $conn->prepare("SELECT * FROM `prodotto` WHERE Codice_prodotto IN (SELECT FK_Codice_prodotto
                                                                                                    FROM `singolo_acquisto`
                                                                                                    WHERE FK_Codice_acquisto='$codice_acquisto')");
                $sql_prodotto->execute();
                $prodotti = $sql_prodotto->get_result();
                $sql_prodotto->close();
                if($prodotti->num_rows > 0){
                    while($prodotto = $prodotti->fetch_assoc()){
                        $name = $prodotto["Nome"];
                        $price = $prodotto["Importo"];
                        echo "<div class='row'>
                                <div class='col-8'>$name</div>
                                    <div class='col-4'>$price$</div>
                                </div>";
                    }
                }  
            }
        }
        echo "</div>";
        }
    ?>
    <h2>Conclusi</h2>
    </br>
    <h2>Nel Carrello</h2>
    <?php
        $user = $_SESSION["username"];
        
        $sql = $conn->prepare("SELECT * FROM `singolo_acquisto` WHERE FK_Codice_acquisto IN (SELECT Codice_acquisto 
                                                                              FROM `acquisto`
                                                                              WHERE FK_UserName=?)");
        $sql->bind_param('s', $user);
        $sql->execute();
        $acquisti = $sql->get_result();
        $sql->close();
        echo "<div class='container'>";
  
        if($acquisti->num_rows > 0){
          while($singolo_acquisto = $acquisti->fetch_assoc()){
            $codice_prodotto = $singolo_acquisto["FK_Codice_prodotto"];
            $sql_prodotto = $conn->prepare("SELECT * FROM `prodotto` WHERE Codice_prodotto='$codice_prodotto'");
            $sql_prodotto->execute();
            $prodotto = $sql_prodotto->get_result()->fetch_assoc();
            $sql_prodotto->close();
            $name = $prodotto["Nome"];
            $price = $prodotto["Importo"];
            echo "<div class='row'>
                    <div class='col-8'>$name</div>
                    <div class='col-4'>$price$</div>
                  </div>";
          }
          echo "<button type='button' class='btn btn-primary'>Checkout</button>
              </div>";
        } else {
          echo "Il tuo carrello è vuoto al momento";
        }

        require_once("footer.html");
    ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>