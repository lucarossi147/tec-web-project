<?php
	include 'functions.php';
	sec_session_start();
?>
<!DOCTYPE html>
<html lang="it">
<head>
	<title>Signup - Event.ino</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<!-- form Css -->
	<link rel="stylesheet" href="css/form.css">
<script  src="sha512.js"></script>
<script  src="forms.js"></script>
</head>
<body>
	


<form class="form-container-sig" method="post" action="./signup_handler.php">
	<span>
		<a href="index.php">
			<img class="index-img" src="images/icons/logo_transparent_c.png" alt="Event.ino">
		</a>
	</span>
	<?php
		if(isset($_SESSION["err_user"])){
			echo('<p class="err-message">'.$_SESSION["err_user"].'<br/></p>');
			unset($_SESSION["err_user"]);
		}
	?>
  <div class="form-group">
    <label for="exampleInputName">Nome</label>
    <input type="text" name="nome" class="form-control" id="exampleInputName" placeholder="nome" aria-describedby="emailHelp" required>
  </div>
  <div class="form-group">
    <label for="exampleInputCognome">Cognome</label>
    <input type="text" name="cognome" class="form-control" id="exampleInputCognome" placeholder="cognome" aria-describedby="emailHelp" required>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Email</label>
    <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="email" aria-describedby="emailHelp" required>
    <small id="emailHelp" class="form-text text-muted">Usa la tua mail principale!</small>
  </div>
  <div class="form-group">
    <label for="exampleInputUsername">Username</label>
    <input type="text" name="username" class="form-control" id="exampleInputUsername" placeholder="username" aria-describedby="emailHelp" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" name="pass" class="form-control" id="exampleInputPassword1" placeholder="password" required>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword2">Conferma password</label>
    <input type="password" name="pass2" class="form-control" id="exampleInputPassword2" placeholder="password" required>
  </div>
  <div class="form-group">
    <label for="exampleInputCellulare">Cellulare</label>
    <input type="tel" name="cell" class="form-control" id="exampleInputCellulare" placeholder="cellulare" required>
  </div>
  
  <button type="submit" class="btn btn-primary" onclick="formhashsignup(this.form, this.form.pass, this.form.pass2);">Submit</button>
</form>

<!--

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
				<form class="login100-form validate-form" method="post"  action="./signup_handler.php"  autocomplete="on" onsubmit="return false;">
					<span class="login100-form-title p-b-33">
					<a href="index.php">
						<img class="login-img" src="images/icons/logo_transparent_c.png" alt="Event.ino">
					</a>
					</span>
					
                  	<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="nome" placeholder="Nome" required>
					</div>
                  
                  	<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="cognome" placeholder="Cognome" required>
					</div>
                  
                  	<br/>
                  
					<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="email" placeholder="Email" required>
					</div>
                  
                  	<div class="wrap-input100 validate-input" >
						<input class="input100" type="text" name="username" placeholder="Username" required>
					</div>
					<div class="wrap-input100 rs1 validate-input" autocomplete="off">
						<input class="input100" type="password" name="pass" placeholder="Password" required>
                    </div>
                    
                    <div class="wrap-input100 rs1 validate-input"  autocomplete="off">
						<input class="input100" type="password" name="pass2" placeholder="Stessa password" required>
					</div>
                  
                  	<div class="wrap-input100 rs1 validate-input" data-validate="Inserisci una password">
						<input class="input100" type="tel" name="cell" placeholder="Cellulare" required>
                    </div>

					<div class="container-login100-form-btn m-t-20">
						<button class="login100-form-btn" >
							Sign up
						</button>
					</div>
                    
				</form>
			</div>
		</div>
	</div>
					-->
</body>
</html>