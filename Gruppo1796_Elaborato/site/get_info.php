<?php 
        //Db connection
        require_once("connection.php");

        if($stmt= $conn->prepare("SELECT * FROM evento WHERE Codice_evento=?")){

            //Bind 
            $stmt->bind_param("s",$_POST['Codice_evento']);
            
            //Execute
            $stmt->execute();

            //Ottieni result
            $result = $stmt->get_result();

            //Estraggo risultato -> codice manifestazione
            $row = $result->fetch_array(MYSQLI_NUM);
            $indirizzo_struttura = $row[1];
            $nome_struttura = $row[2];
            $codice_manifestazione = $row[3];
            $nome_evento = $row[4];
            $img_evento = $row[5];
            $descrizione = $row[6];
            $num_ospiti = $row[7];
            $posti_disponibili = $row[8];
            $biglietti = $row[9];
            $data_evento = $row[10];
            $categoria_evento = $row[11];
            
            $change = array();
            $result=array(  'indirizzo_struttura' => $indirizzo_struttura,
                            'nome_struttura' => $nome_struttura,
                            'codice_manifestazione'=> $codice_manifestazione,
                            'nome_evento' => $nome_evento,
                            'img_evento'=> $img_evento,
                            'descrizione'=> $descrizione,
                            'num_ospiti'=> $num_ospiti,
                            'posti_disponibili'=>$posti_disponibili,
                            'biglietti'=> $biglietti,
                            'data_evento'=> $data_evento,
                            'categoria_evento'=> $categoria_evento);
            echo json_encode($result);
        }
    ?>