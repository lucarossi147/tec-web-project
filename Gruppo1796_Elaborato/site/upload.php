<?php
$directory = "/images/";
$file = $directory . basename($_FILES["fileToUpload"]["name"]);
$result_upload = 1;
$tipo_img = strtolower(pathinfo($file,PATHINFO_EXTENSION));

//Controlla se il file sia una immagine
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
    if($check !== false) {
        //echo "Il file è una immagine " . $check["mime"] . ".";
        $result_upload = 1;
    } else {
        echo "Il file non è una immagine!";
        $result_upload = 0;
    }
}

//Controlla se il file non sia già presente
if (file_exists($file)) {
    echo "Attenzione il file è già esistente";
    $result_upload = 0;
}

// Controlla dimensione immagine
if ($_FILES["fileToUpload"]["size"] > 500000) {
    echo "Attenzione il suo file è troppo grande";
    $result_upload = 0;
}

// Controlla formato file
if($tipo_img != "jpg" && $tipo_img != "png" && $tipo_img != "jpeg"
&& $tipo_img != "gif" ) {
    echo "Attenzione solo i file JPG, JPEG, PNG & GIF sono permessi";
    $result_upload = 0;
}

// Controlla risultato upload
if ($result_upload == 0) {
    echo "File non caricato";

}  else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $file)) {
        echo "Il file ". basename( $_FILES["fileToUpload"]["name"]). " è stato caricato";
        //Redirect
        header("location: insert_manifest.php");
    } else {
        echo "Errore nel caricamento";
        echo "<a href='upload_photo.php'>Torna</p>";
    }
}
               
?>