<?php
    include 'functions.php';
    sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

    <!-- Main Css -->
    <link rel="stylesheet" href="css/main.css">
    <title>Evento | Event.ino</title>

    <script src="cart_btn.js"></script>
  </head>

  <body>

    <!-- 
        Header 
    -->
    <?php
      require_once("header.php");
    ?>

    <!-- Retrieve data from db -->
    <?php

    require_once("connection.php");
    if(isset($_GET['event'])){
      $num_event=$_GET["event"];  
    } else {
      //Redirect
      header("location: events.php");
    }
    
    $stmt = $conn->prepare("SELECT * FROM `evento` WHERE Codice_evento=?");
    $stmt->bind_param('i',$num_event);
    $stmt->execute();
    $result = $stmt->get_result();
    $stmt->close();

    if ($result->num_rows > 0) {
        // output data of each row, usually only one row
        while($row = $result->fetch_assoc()) {
            $title=$row["Nome_evento"];
            $desc=$row["Descrizione"];
            $img=$row["Img_evento"];
        }

        echo "<div>" . "\n";
        echo "<div>"."\n";
        echo "<h1> 
                $title
              </h1>"."\n";        
        echo "<div>" . "\n";
        echo "<div class='row'>"."\n";
        echo "<div class='col'>"."\n";
        echo "<div class='row'>"."\n";
        echo "<img class='card-img-top img-fluid' src='$img' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>";
        echo "</div>" . "\n";
        echo "<div class='row'>"."\n";
    } else {
        //Redirect
      header("location: events.php");
    }
    
    $stmt = $conn->prepare("SELECT * FROM `prodotto` WHERE FK_Codice_evento=? AND Nel_carrello='false'");
    $stmt->bind_param('i',$num_event);
    $stmt->execute();
    $result = $stmt->get_result();
    $stmt->close();
    echo "<div class='container'>
            <h2>Biglietti e abbonamenti disponibili</h2>";            
    if($result->num_rows>0){
      for($i=0;$i<$result->num_rows;$i++){
        $prodotto = $result->fetch_assoc();
        $name = $prodotto["Nome"];
        $price = $prodotto["Importo"];
        $id = $prodotto["Codice_prodotto"];
        echo "<div class='row'>
                  <div class='col-8'> $name</div>
                  <div class='col-4'>$price$</div>";
        if(login_check($conn)){
          echo"<div class='col'>
                  <button type='button' class='btn btn-primary' onclick='modifyCart(`$id`,`$i`)' id='cart_btn $i'>+</button>
                </div>";
        }
       
        echo" </div>";
      }


    echo "</div>";
    echo "</div>
              </div>
              <div class='col'>
                <div class='row desc-block'>
                  <h2>Descrizione Evento</h2>
                  <h1> $desc</h1>
                </div>
                <div class='row'>";
    } else {
      echo "Evento sold out...";
    }

    
    $stmt = $conn->prepare("SELECT FK_Nome_ospite FROM `partecipazione` WHERE FK_Codice_evento=?");
    $stmt->bind_param('i',$num_event);
    $stmt->execute();
    $result = $stmt->get_result();
    $stmt->close();

    if($result->num_rows > 0){
      while($guest = $result->fetch_assoc()){
        $guest_name = $guest["FK_Nome_ospite"];
        echo "<div class='row'>
                <div class='col'>
                  $guest_name
                </div>
              </div>";
      }
    }
    echo"        </div>
              </div>
            </div>
          </div>
          </div>
          </div>
          </div>"."\n";     
    require_once("footer.html");
    ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>

</html>