<?php
  include 'functions.php';
  sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

    <title>Categorie</title>
  </head>
  <body>

    <?php
        require_once("header.php");
    ?>
    <div>
        <?php

            require_once("connection.php");
            
            $carousel_dim = 3;

            //Get every event category
            $sql_categories = $conn->prepare("SELECT * FROM `categoria_evento`");
            $sql_categories->execute();
            $result_category = $sql_categories->get_result();
            $sql_categories->close();
            if ($result_category->num_rows > 0) {
                // output data of each row, multiple row
                while($category = $result_category->fetch_assoc()) {
                    $name = $category["Nome_categoria_evento"];
                    echo "<h2> $name </h2>";
                    
                    //For every category searches the top events and display them in a Carousel
                    
                    $sql_events = $conn->prepare("SELECT * FROM `evento` WHERE FK_Nome_categoria_evento =? LIMIT $carousel_dim");
                    $sql_events->bind_param('s', $name);
                    $sql_events->execute();
                    $result_events = $sql_events->get_result();
                    $sql_events->close();
                    if($result_events ->num_rows > 0) {
                        //Create an array of events and images and fill them
                        $events = array();
                        $images = array();
                        for($i=0; $i < $carousel_dim; $i++){
                            $events[$i] =  $result_events -> fetch_assoc();
                        }
                        //Echo a carousel with the data acquired
                        //Indicators:
                        echo "<div id='eventCarousel' class='carousel slide' data-ride='carousel'>
                        <div class='carousel-inner'>";
                        
                        //The first Carousel item, must be set active
                        $name = $events[0]['Nome_evento'];
                        $desc = $events[0]['Descrizione'];
                        $event_id = $events[0]['Codice_evento'];
                        $image = $events[0]["Img_evento"];
                        $stmt = $conn->prepare("SELECT Importo FROM `prodotto` WHERE FK_Codice_evento=? ORDER BY Importo asc LIMIT 1");
                        $stmt->bind_param('i', $event_id);
                        $stmt->execute();
                        $result = $stmt->get_result();
                        $stmt->close();
                        $price=NULL;
                        if (isset($result)) {
                            $ticket = $result->fetch_assoc();
                            $price=$ticket["Importo"];
                        } else {
                            $price = "err";
                        }
                        //Each carousel item is a card with the event infos
                        echo"<div class='carousel-item active' data-ride='carousel'>
                                <a href='event-page.php?event=$event_id' class='card-link'>
                                    <img class='carousel-img card-img-top img-fluid' src='$image' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>    
                                </a>
                                <div class='card-body text-center'>
                                    <h4 class='card-title'> $name </h4>
                                    <p class='card-text'> $desc </p>
                                    <a href='event-page.php?event=$event_id' class='card-link'>
                                        <button type='button' class='btn btn-secondary'>A partire da $price $</button>
                                    </a>
                                </div>
                            </div>";

                        //The remaining Carousel items
                        for($i=1; $i < $carousel_dim; $i++){
                            $name = $events[$i]['Nome_evento'];
                            $desc = $events[$i]['Descrizione'];
                            $event_id = $events[$i]['Codice_evento'];
                            $image = $events[$i]["Img_evento"];
                            $sql = "SELECT Importo FROM `prodotto` WHERE FK_Codice_evento=$event_id ORDER BY Importo desc LIMIT 1";
                            $result = $conn->query($sql);
                            $price=NULL;
                            if (isset($result)) {
                                $ticket = $result->fetch_assoc();
                                $price=$ticket["Importo"];
                            } else {
                                $price = "err";
                            }
                            echo"<div class='carousel-item' data-ride='carousel'>
                                    <a href='event-page.php?event=$event_id' class='card-link'>
                                        <img class='carousel-img card-img-top img-fluid ' src='$image' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>    
                                    </a>    
                                    <div class='card-body text-center'>
                                        <h4 class='card-title'> $name </h4>
                                        <p class='card-text'> $desc </p>
                                        <a href='event-page.php?event=$event_id' class='card-link'>
                                            <button type='button' class='btn btn-secondary'>A partire da $price $</button>
                                        </a>
                                    </div>
                                </div>";
                        }

                        //Carousel navigation arrows
                        echo "</div>            
                                <a class='carousel-control-prev' href='#eventCarousel' role='button' data-slide='prev'>
                                    <span class='carousel-control-prev-icon' aria-hidden='true'></span>
                                    <span class='sr-only'>Previous</span>
                                </a>
                                    
                                <a class='carousel-control-next' href='#eventCarousel' role='button' data-slide='next'>
                                    <span class='carousel-control-next-icon' aria-hidden='true'></span>
                                    <span class='sr-only'>Next</span>
                                </a>
                            </div>";
                    } else {
                        echo "<p> Nessun evento trovato per questa categoria </p>";
                    }
                }
            } else {
                echo "0 results";
            }
            $conn->close();
            
            //Events page link
            echo "<a class='before-footer' href='events.php'> Visualizza tutti gli eventi </a>";

        ?>
    </div>

    <?php
        require_once("footer.html");
    ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>