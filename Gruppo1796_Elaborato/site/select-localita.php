<?php
    include 'functions.php';
    require_once("connection.php");

    if(isset($_POST['regione'])){
        $regione=$_POST['regione'];
    }
    
    if(isset($_POST['provincia'])){
        $provincia=$_POST['provincia'];
        $sql = "SELECT DISTINCT `Nome_localita` FROM `localita_` WHERE `Provincia`='$provincia' ORDER BY `localita_`.`Nome_localita` ASC";
        $result = $conn->query($sql); 
        if ($result->num_rows > 0) {
            $localita=array();
            while($row = $result->fetch_assoc()) { 
                array_push($localita,$row);
            }
            echo json_encode($localita);  
        }
        else {  
                echo "0 result";
        }
    }else{
        $sql = "SELECT DISTINCT `Provincia` FROM `localita_` WHERE `Regione`='$regione' ORDER BY `localita_`.`Regione` ASC";
        $result = $conn->query($sql); 
        if ($result->num_rows > 0) {
            $province=array();
            while($row = $result->fetch_assoc()) { 
                array_push($province,$row);
            }
            echo json_encode($province);  
        } else {
            echo "0 results";
        } 
    }
?>
