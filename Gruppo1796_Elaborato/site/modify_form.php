<div class="form-row">
            <!--Insert nome-->
            <div class="col">
                <label for="event_name">Nome evento:</label>
                <input value ="" type="text" class="form-control" id="event_name" placeholder="Nome evento" autocomplete="off" name="event_name" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci il nome dell'evento</div>
            </div>
            <!--Insert immagine-->
            <div class="col">
                <label for="event_image">Percorso locandina:</label>
                <input type="text" class="form-control" id="event_image" placeholder="Percorso immagine" name="event_image" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci immagine</div>
            </div>
</div>
        <!--Insert descrizione ( text area ) -->
        <div class="form-group">
            <label class="label-text-area" for="descrizione_evento">Descrizione manifestazione</label>
            <textarea class="form-control" value="" id="descrizione_evento" rows="5" 
            name="event_desc" placeholder="Inserisci descrizione evento" required></textarea>
            <div class="valid-feedback"> Valido </div>
            <div class="invalid-feedback">Perfavore, inserisci la descrizione</div>
        </div>
        <div class="form-row">
            <!--Insert posti-->
            <div class="col">
                <label for="posti_evento">Posti disponibili:</label>
                <input type="numeric" class="form-control" id="posti_evento" placeholder="0" name="posti_evento" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci numero posti</div>
            </div>
            <!--Insert biglietti-->
            <div class="col">
                <label for="biglietti_evento">Biglietti venduti:</label>
                <input type="numeric" class="form-control" id="biglietti_evento" placeholder="0" 
                name="biglietti_evento" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci biglietti venduti</div>
            </div>
        </div>
        <div class="form-row">
            <!-- Selezione tipologia evento --> 
            <div class="col">
                <label>Tipologia evento</label>
                <select class="form-control" id="form-selected-tipo" name="categoria_evento"> 
                <?php
                    $sql = "SELECT Nome_categoria_evento FROM categoria_evento";
                    $result = $conn->query($sql);    
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { echo '<option>' . $row['Nome_categoria_evento'] . '</option>';}
                    } else {
                        echo "0 results";
                    } 
                ?>
                </select>
            </div>
            <!-- Selezione struttura --> 
            <div class="col">
                <label> Struttura </label>
                <select class="form-control" id="form-selected-struttura" name="nome_struttura"> 
                <?php
                    $sql = "SELECT `Nome_struttura` FROM `struttura`";
                    $result = $conn->query($sql);    
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { echo '<option>' . $row['Nome_struttura'] . '</option>';}
                    } else {
                        echo "0 results";
                    }  
                ?>
                </select>
            </div>
        </div>
        <div class="form-row">
        <!-- Selezione manifestazione --> 
            <div class="col">
                <label> Nome manifestazione </label>
                <select class="form-control" id="form-selected-manifest" name="nome_manifestazione"> 
                <?php
                    $sql = "SELECT Nome_manifestazione FROM manifestazione";
                    $result = $conn->query($sql);    
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { echo '<option>' . $row['Nome_manifestazione'] . '</option>';}
                    } else {
                        echo "0 results";
                    }  
                ?>
                </select>
            </div>
            <!--Insert num.ospiti-->
            <div class="col">
                <label for="num_ospiti">Numero ospiti:</label>
                <input type="numeric" class="form-control" id="num_ospiti" placeholder="0" 
                name="num_ospiti" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci numero ospiti</div>
            </div>
        </div>
        
        <!-- Scelta data -->
        <div class="form-row">
                <label for="data_evento">Data evento:</label>
                <input type="text" name="data_evento" class="datepicker" id="datepicker" required/>
        </div>

        <!-- Submit -->
        <div class="form-group"> 
            <?php 
                if(isset($_SESSION["org"]) || isset($_SESSION["admin"])){
                    echo "<button formaction='modify_event_script.php' 
                    formmethod='POST' type='submit' class='btn btn-primary mb-2'>Inserisci</button>";
                }else{
                    echo "<label class='red-alert' for='button-submit'>Non hai i permessi</label>
                    <button disabled formaction='modify_event_script.php' 
                    formmethod='POST' id='button-submit' type='submit' class='btn btn-primary mb-2'>Inserisci</button>";
                }
            ?>
        </div>
</form>