<header>
    <?php
      require_once("navbar.php");
    ?>

    <form action="./search_handler.php">
      <div class="form-row align-items-center">
        <div class="col-auto">
          <input title="Insert requested event" class="form-control mr-sm-2" name="search" type="search" placeholder="Search" aria-label="Search">
        </div>
        <div class="col-auto">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </div>
      </div>
       
    </form>

</header>