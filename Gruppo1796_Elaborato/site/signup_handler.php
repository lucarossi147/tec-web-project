<?php
    include 'functions.php';
    sec_session_start();
    $server="localhost";
    $usr="root";
    $pwd="";
    $db="eventino_db";

    $incomplete = 0;

    if (isset($_POST)){
        $nome = htmlentities($_POST['nome'], ENT_QUOTES);
        $cognome = htmlentities($_POST['cognome'], ENT_QUOTES);
        $email = htmlentities($_POST['email'], ENT_QUOTES);
        $username = htmlentities($_POST['username'], ENT_QUOTES);
        $cellulare = htmlentities($_POST['cell'], ENT_QUOTES);
        $password = htmlentities($_POST['p'], ENT_QUOTES);
        $mysqli = new mysqli($server,$usr,$pwd,$db);

        if ($mysqli -> connect_errno) {
            echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
            exit();
        }

        //controllo non ci sia gia' qualcuno con questo username
        $stmt = $mysqli->prepare("SELECT UserName, Mail FROM `utente` WHERE (UserName=? OR Mail=?) ");
        $stmt->bind_param('ss', $username, $email);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->bind_result($username, $email); // recupera il risultato della query e lo memorizza nelle relative variabili.
        $stmt->fetch();
        $stmt->close();
        if($result->num_rows == 0 ){
            $stmt = $mysqli->prepare("INSERT INTO `utente` (`Nome`, `Cognome`, `Mail`, `UserName`, `Password`,`sale`, `Img_utente`, `Cellulare`, `Organizzatore`, `Amministratore`, `Disabilitato`) 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                $stmt->bind_param('sssssssiiii', $nome, $cognome, $email, $username, $pass, $sale, $im_ut, $cellulare, $org, $amm, $dis);
                $bytes = openssl_random_pseudo_bytes(LUNGHEZZA_SALE); //c'e' un magic number!!!!!!
                $sale = bin2hex($bytes);
                $pass = hash('sha512', $password.$sale);
                $im_ut = NULL;
                $org = '0';
                $amm = '0';
                $dis = '0';
                $stmt->execute();
                $stmt->close();

                echo "ok";

                //se va a buon fine faccio var session, e vado su index, 
                //su index controllo e avviso che e' andato a buon fine e dico di loggare
                $_SESSION["signup"]="Signup eseguito con successo, ora puoi effettuare login";
                $msg = "Benvenuto a far parte della nostra community, speriamo tu possa divertirti un mondo grazie al nostro portale";
                // Invia un e-mail all'utente avvisandolo che il suo account è stato disabilitato.
                sendmail($email,"Benvenuto in Event.ino", $msg);
                //faccio fare il login
                login($username, $password, $mysqli);
                header("Location:index.php");
                exit;
        }else{
            $_SESSION["err_user"]="email o username già in uso";
            header("Location:signup.php");
        }
        $mysqli->close();
     
    }
?>