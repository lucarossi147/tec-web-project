<div>
    <?php
        require_once("connection.php");
        $carousel_dim;

        //echo "<h2>Potrebbe interessarti...</h2>";
        $utente = $_SESSION["username"];

        $feeded_events = array();
        //Get every event in which participates a guest the user follows and display it in a carousel
        if($sql_events = $conn->prepare(" SELECT * FROM evento WHERE Codice_evento IN (SELECT FK_Codice_evento 
                                                                        FROM partecipazione 
                                                                        WHERE FK_Nome_ospite IN (SELECT FK_Nome_ospite 
                                                                                                    FROM segue
                                                                                                    WHERE FK_UserName=?))")){
        
        //Bind param
        $sql_events->bind_param('s', $utente);
        
        //Execute query
        $sql_events->execute();
        
        //Get the result of the query
        $events = $sql_events->get_result();
        
        //Close statement
        $sql_events->close();
                                                                                            
        }else {  
            //Errore selezione eventi seguiti
            //Se avviene un errore, stampo il codice e stampo link per redirect
            $error = $conn->errno . ' ' . $conn->error;
            $console.log($error);
        }
        
        if($events->num_rows > 0){
            echo "<h2 class='card-title' >Ciao $utente, potrebbe interessarti...</h2>";
            for($i=0;$i<$events->num_rows;$i++){
                $feeded_events[$i] = $events->fetch_assoc();
            }
            $carousel_dim = sizeof($feeded_events);
            //Echo a carousel with the data acquired
            //Indicators:
            echo "<div id='feedCarousel' class='carousel slide' data-ride='carousel'>
            <div class='carousel-inner'>";
                    
            for($i=0; $i < $carousel_dim; $i++){
                $name = $feeded_events[$i]['Nome_evento'];
                $desc = $feeded_events[$i]['Descrizione'];
                $event_id = $feeded_events[$i]['Codice_evento'];
                $image = $feeded_events[$i]["Img_evento"];
                $stmt = $conn->prepare("SELECT Importo FROM `prodotto` WHERE FK_Codice_evento=? ORDER BY Importo asc LIMIT 1");
                $stmt->bind_param('i', $event_id);
                $stmt->execute();
                $result = $stmt->get_result();
                $stmt->close();
                $price=NULL;
                if (isset($result)) {
                    $ticket = $result->fetch_assoc();
                    $price=$ticket["Importo"];
                } else {
                    $price = "err";
                }
                if($i==0){//The first Carousel item, must be set active
                    echo"<div class='carousel-item active' data-ride='carousel'>
                        <a href='event-page.php?event=$event_id' class='card-link'>
                            <img class='card-img-top img-fluid' src='$image' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>    
                        </a>
                        <div class='card-body text-center'>
                            <h4 class='card-title'> $name </h4>
                            <p class='card-text'> $desc </p>
                            <a href='event-page.php?event=$event_id' class='card-link'>
                                <button type='button' class='btn btn-secondary'>A partire da $price $</button>
                            </a>
                        </div>
                    </div>";
                } else {
                    echo"<div class='carousel-item' data-ride='carousel'>
                        <a href='event-page.php?event=$event_id' class='card-link'>
                            <img class='card-img-top img-fluid' src='$image' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>    
                        </a>    
                        <div class='card-body text-center'>
                            <h4 class='card-title'> $name </h4>
                            <p class='card-text'> $desc </p>
                            <a href='event-page.php?event=$event_id' class='card-link'>
                                <button type='button' class='btn btn-secondary'>A partire da $price $</button>
                            </a>
                        </div>
                    </div>";
                }
                
            }

            //Carousel navigation arrows
            echo "</div>            
                    <a class='carousel-control-prev' href='#feedCarousel' role='button' data-slide='prev'>
                        <span class='carousel-control-prev-icon' aria-hidden='true'></span>
                        <span class='sr-only'>Previous</span>
                    </a>
                                
                    <a class='carousel-control-next' href='#feedCarousel' role='button' data-slide='next'>
                        <span class='carousel-control-next-icon' aria-hidden='true'></span>
                        <span class='sr-only'>Next</span>
                    </a>
                </div>";
        }
        
    ?>
</div>