<?php
	include 'functions.php';
	sec_session_start();
?>
<!DOCTYPE html>
<html lang="it">
<head>
	<title>Login - Event.ino</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
<!--===============================================================================================-->
	<!--<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
===============================================================================================-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

	<!-- form Css -->
	<link rel="stylesheet" href="css/form.css">

<script src="sha512.js"></script>
<script src="forms.js"></script>
</head>
<body>


<form class="form-container-log" method="post" action="./login_handler.php">
	<span>
		<a href="index.php">
			<img class="index-img" src="images/icons/logo_transparent_c.png" alt="Event.ino">
		</a>
	</span>
	<?php
		if(isset($_SESSION["error"])){
			echo('<br/><p class="err-message">'.$_SESSION["error"].'<br/></p>');
			unset($_SESSION["error"]);
		}
		if(isset($_SESSION["disabilitato"])){
			echo('<br/><p class="err-message">'.$_SESSION["disabilitato"].'<br/></p>');
			unset($_SESSION["disabilitato"]);
		}
	?>
  <div class="form-group">
    <label for="exampleInputEmail1">Username o email</label>
    <input type="text" name="email"  class="form-control" id="exampleInputEmail1" placeholder="username/email">
    <small id="emailHelp" class="form-text text-muted">Non condivieremo mai la tua mail con nessuno</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" name="pass" class="form-control" id="exampleInputPassword1" placeholder="password">
  </div>
  
  <button type="submit" class="btn btn-primary" onclick="formhash(this.form, this.form.pass);">Submit</button>

  <div class="text-center sig-link">
		<span class="txt1">
		Create an account 
		</span>

		<a href="./signup.php" class="txt2 hov1">
			Sign up
		</a>
	</div>
</form>


<!--
	<div class="form-group form-check">
    <input type="checkbox" class="form-check-input" id="exampleCheck1">
    <label class="form-check-label" for="exampleCheck1">Check me out</label>
  </div> -->
</body>
</html>
