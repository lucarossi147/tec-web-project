<?php
include 'functions.php';
sec_session_start();
require_once("connection.php");

if(!login_check($conn)){
    exit();
}

if(isset($_POST['carta'])){
    $numero_carta=$_POST['carta'];
}

if(isset($_POST['scadenza'])){
    $scadenza=$_POST['scadenza'];
}

if(isset($_POST['nonce'])){
    $nonce=$_POST['nonce'];
}

if(isset($_POST['finali'])){
    $finali=$_POST['finali'];
}

if(isset($_SESSION['username'])){
    $usr=$_SESSION['username'];
}

$predefinito=0;

if($stmt= $conn->prepare("INSERT INTO `metodo_di_pagamento`(`Numero_carta`, `Scadenza`, 
    `predefinito`, `Chiave`, `Finali`) VALUES (?,?,?,?,?)")){
        
        //Bind dei parametri
        $stmt->bind_param("ssiss", $numero_carta, $scadenza, $predefinito, $nonce, $finali);
            
        //Eseguo query
        $stmt->execute();

        //Ottengo risultato
        $result = $stmt->insert_id;

        printf("Errorcode: %d\n", $conn->errno);
        printf("Error: %s\n", $conn->error);
} else { 
    //Errore insert
    $error = $conn->errno . ' ' . $conn->error;
    echo $error;
    exit();
}

if($stmt= $conn->prepare("INSERT INTO `possedimento`(`FK_Codice_carta`, `FK_UserName`) VALUES (?,?)")){
        
        //Bind dei parametri
        $stmt->bind_param("ss", $result, $usr );
            
        //Eseguo query
        $stmt->execute();

        $mia_mail = $_SESSION['mail'];
        sendmail($mia_mail, "Carta aggiunta con successo", "La tua carta e' stata aggiunta correttamente");
        printf("Errorcode: %d\n", $conn->errno);
        printf("Error: %s\n", $conn->error);
} else { 
    //Errore insert
    $error = $conn->errno . ' ' . $conn->error;
    echo $error;
    exit();
}
?>