RIEMPIMENTO DB
//creare utente Event.ino (Event., ino, Event.ino)


//ATTENZIONE:
Mi sono scordato di salvare le query di creazione manifestazione, create 4 manifestazioni con FK_UserName Event.ino e date i codici in questo modo:
10 concerti
11 mostre
12 sport
13 teatro
NB VEDI FOTO SU WHATSAPP


//STRUTTURE
PRIME DUE
INSERT INTO `struttura` (`Indirizzo_struttura`, `Nome_struttura`, `FK_Localita`, `Img_struttura`, `Capienza`, `Posti_disponibili`) VALUES ('via stadio di prova 1', 'Stadio di prova', '1797', NULL, '80000', '80000'), ('via teatro di prova', 'Teatro di prova', '3994', NULL, '1000', '1000');

PENULTIMA
INSERT INTO `struttura` (`Indirizzo_struttura`, `Nome_struttura`, `FK_Localita`, `Img_struttura`, `Capienza`, `Posti_disponibili`) VALUES ('via museo di prova 3', 'Museo di prova', '7834', NULL, '1000', '5000');

ULTIMA
INSERT INTO `struttura` (`Indirizzo_struttura`, `Nome_struttura`, `FK_Localita`, `Img_struttura`, `Capienza`, `Posti_disponibili`) VALUES ('via dello stadio secondo', 'Stadio secondo di prova', '3984', NULL, '20000', '20000');

//EVENTI

SPORTIVI
INSERT INTO `evento` (`Codice_evento`, `FK_Indirizzo_struttura`, `FK_Nome_struttura`, `FK_Codice_manifestazione`, `Nome_evento`, `Img_evento`, `Descrizione`, `Num_ospiti`, `Posti_disponibili`, `Biglietti_venduti`, `Data_evento`, `FK_Nome_categoria_evento`) VALUES (NULL, 'via stadio di prova 1', 'Stadio di prova', '12', 'Partita sportiva di prova 1', 'images/stdimages/sport-1.jpg', '', '2', '80000', '0', '2020-01-12', 'Eventi Sportivi'), (NULL, 'via stadio di prova 1', 'Stadio di prova', '10', 'Partita sportiva di prova 2', 'images/stdimages/sport-2.jpg', '', '2', '80000', '0', '2020-01-19', 'Eventi Sportivi');

MOSTRE
INSERT INTO `evento` (`Codice_evento`, `FK_Indirizzo_struttura`, `FK_Nome_struttura`, `FK_Codice_manifestazione`, `Nome_evento`, `Img_evento`, `Descrizione`, `Num_ospiti`, `Posti_disponibili`, `Biglietti_venduti`, `Data_evento`, `FK_Nome_categoria_evento`) VALUES(NULL, 'via museo di prova 3', 'Museo di prova', '11', 'Mostra di prova 1', 'images/stdimages/art-1.jpg', '', '0', '1000', '0', '2020-01-15', 'Mostre'), (NULL, 'via museo di prova 3', 'Museo di prova', '11', 'Mostra di prova 2', 'images/stdimages/art-2.jpg', '', '0', '1000', '0', '2020-01-22', 'Mostre'),(NULL, 'via museo di prova 3', 'Museo di prova', '11', 'Mostra di prova 3', 'images/stdimages/art-3.jpg', '', '0', '1000', '0', '2020-01-29', 'Mostre');

CONCERTI
INSERT INTO `evento` (`Codice_evento`, `FK_Indirizzo_struttura`, `FK_Nome_struttura`, `FK_Codice_manifestazione`, `Nome_evento`, `Img_evento`, `Descrizione`, `Num_ospiti`, `Posti_disponibili`, `Biglietti_venduti`, `Data_evento`, `FK_Nome_categoria_evento`) VALUES(NULL, 'via dello stadio secondo', 'Stadio secondo di prova', '10', 'Concerto di prova 1', 'images/stdimages/concerto-1.jpg', '', '1', '20000', '0', '2020-01-10', 'Concerti'), (NULL, 'via dello stadio secondo', 'Stadio secondo di prova', '10', 'Concerto di prova 2', 'images/stdimages/concerto-2.jpg', '', '1', '20000', '0', '2020-01-17', 'Concerti'),(NULL, 'via dello stadio secondo', 'Stadio secondo di prova', '10', 'Concerto di prova 3', 'images/stdimages/concerto-3.jpg', '', '1', '20000', '0', '2020-01-24', 'Concerti');

SPETTACOLI
INSERT INTO `evento` (`Codice_evento`, `FK_Indirizzo_struttura`, `FK_Nome_struttura`, `FK_Codice_manifestazione`, `Nome_evento`, `Img_evento`, `Descrizione`, `Num_ospiti`, `Posti_disponibili`, `Biglietti_venduti`, `Data_evento`, `FK_Nome_categoria_evento`) VALUES (NULL, 'via teatro di prova', 'Teatro di prova', '13', 'Spettacolo di prova 1', 'images/stdimages/teatro-1.jpg', '', '1', '1000', '0', '2020-01-11', 'Spettacoli'),(NULL, 'via teatro di prova', 'Teatro di prova', '13', 'Spettacolo di prova 2', 'images/stdimages/teatro-2.jpg', '', '1', '1000', '0', '2020-01-18', 'Spettacoli'),(NULL, 'via teatro di prova', 'Teatro di prova', '13', 'Spettacolo di prova 3', 'images/stdimages/teatro-3.jpg', '', '1', '1000', '0', '2020-01-25', 'Spettacoli');

