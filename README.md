# tec web

TODO:

Tradurre contact
Aggiustare pagina carrello

INSERT INTO `localita` (`Nome_localita`, `Provincia`, `Regione`, `CAP`) VALUES ('Montemarciano', 'AN', 'Marche', '60014');

INSERT INTO `struttura` (`Indirizzo_struttura`, `Nome_struttura`, `Img_struttura`, `Capienza`, `Posti_disponibili`, `FK_CAP`) VALUES ('Via falconara 46', 'Falegnami Stadium', '/path/img/', '10000', '200', '1111');

INSERT INTO `ospite` (`Descrizione`, `Nome_ospite`, `Img_ospite`, `Cantante`, `Ente_Sportivo`, `Artista`, `Figura_Pubblica`) VALUES ('La squadra di calcio più amata dai cesenati, burdel', 'Falegnameria Lucchi', '/paht/img', '0', '1', '0', '0');

INSERT INTO `evento` (`Codice_evento`, `Indirizzo_struttura`, `Nome_struttura`, `FK_Codice_manifestazione`, `Nome_evento`, `Img_evento`, `Descrizione`, `Num_ospiti`, `Luogo_Dell_evento`, `Posti_disponibili`, `Biglietti_venduti`, `Data_evento`, `Concerto`, `Partita`, `Spettacolo`, `Fiera`, `Conferenza`) VALUES ('1', 'Via falconara 46', 'Falegnami', '1', 'Partita falegnami', '/path/img', 'Partita dei falegnami', '1', 'Cesena', '2000', '1234', '2019-12-16', '0', '1', '0', '0', '0')

INSERT INTO `manifestazione` (`Codice_manifestazione`, `FK_UserName`, `Data_inizio`, `Data_fine`, `Num_eventi`, `Nome_manifestazione`, `Descrizione`, `Img_manifestazione`) VALUES ('1', 'SCHIARO98', '2019-09-11', '2020-05-21', '260', 'Campionato CSI', 'Campionato csi calcio a 7', '/path/img/');

INSERT INTO `ricevuta` (`Codice_ricevuta`, `FK_UserName`, `Data_acquisto`, `Ora`, `Prezzo_tot`, `Qta_Biglietti`, `Imponibile`, `Mod_Pagamento`) VALUES ('1', 'SCHIARO98', '2019-12-16', '2350', '89', '2', '22', 'Bancomat');

INSERT INTO `abbonamento` (`Tipo`, `Codice_abbonamento`, `FK_UserName`, `FK_Codice_manifestazione`, `Importo`, `Ingressi`, `Nel_Carrello`) VALUES ('annuale', '1', 'SCHIARO98', '1', '100', '10', '0');

INSERT INTO `biglietto` (`Codice_biglietto`, `FK_Codice_evento`, `FK_Codice_ricevuta`, `FK_Codice_abbonamento`, `FK_UserName`, `Importo`, `Posto_assegnato`, `Nel_Carrello`, `Cartaceo`, `Digitale`) VALUES ('1', '1', '1', '1', 'SCHIARO98', '100', '10', '0', '0', '1');

INSERT INTO `segue` (`Codice_segue`, `FK_Nome_ospite`, `Fk_UserName`) VALUES ('1', 'Falegnameria Lucchi', 'SCHIARO98');