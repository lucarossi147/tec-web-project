Il progetto si propone di creare un database di eventi da accedere tramite relativo sito web.
Un utente qualsiasi pu� fare ricerche in base al luogo, nome dell'evento, ospite principale o citt�. 
Pu� anche registrarsi al sito per effettuare l'acquisto del determinato biglietto per una o pi� persone, gestire il suo carrello o tramite opportuna verifica diventare organizzatore compilando un form da inviare all'amministratore. 
Un amministratore di sistema si occuper� della verifica.
L' organizzatore potr� aggiungere eventi e manifestazioni (intese come insieme di eventi legati da una specifica tematica e che si svolgono in una stessa area )
L'organizzatore dovr� anche poter spostare o annullare gli eventi, che dovranno essere registrati e tenuti in memoria per non pi� di 2 anni, al termine dei quali l'evento andr� eliminato dal database.
Per ogni evento dovranno essere memorizzate: main guest, numero di posti totali, numero di posti occupati, data, luogo, citt�, prezzo per biglietto e se � possibile acquistare un abbonamento per una manifestazione (o un sottoinsieme dei suoi eventi).
Gli eventuali biglietti per singoli eventi o intere manifestazioni potranno essere aggiunti ad un carrello, per poi essere acquistati.
Al termine dell'acquisto occorrer� emettere fattura e relativo biglietto che verranno conservati 10 anni come termini di legge.