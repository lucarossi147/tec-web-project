<nav class="navbar navbar-expand-lg navbar-light bg-light">

    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    
    <a class="navbar-brand" href="index.php">
        <img class ="navbar-brand-img" src="images/icons/logo_transparent_c.png"  alt="Event.ino">
    </a>

    <?php
     require_once("connection.php");
     
        if(login_check($conn)){
            echo "<a class='navbar-item' href='cart-page.php'>
                    <img class='navbar-item-img' src='images/icons/cart.png'  alt='cart'>
                </a>";
        } else {
            echo "<a class='navbar-item' href='login.php'>
                    <img class='navbar-item-img' src='images/icons/user_icon.png'  alt='account'>
                </a>";
        }
    ?>
    
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <a class="nav-item nav-link" href="index.php">Home</a>
        <a class="nav-item nav-link" href="events.php">Eventi</a>
        <a class="nav-item nav-link" href="categories.php">Categorie</a>
        <?php
        if(login_check($conn)){
            echo "<a class='nav-item nav-link' href='user_profile.php'>Visualizza Profilo</a>";
            echo "<a class='nav-item nav-link' href='logout_handler.php'>Logout</a>";
        } else {
            echo "<a class='nav-item nav-link' href='login.php'>Login</a>";
        }
        ?>
    </div>
</nav>