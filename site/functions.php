<?php
define("TASSE", 0.78 );
define("LUNGHEZZA_SALE", 16);

function sec_session_start() {
        $session_name = 'sec_session_id'; // Imposta un nome di sessione
        $secure = false; // Imposta il parametro a true se vuoi usare il protocollo 'https'.
        $httponly = true; // Questo impedirà ad un javascript di essere in grado di accedere all'id di sessione.
        ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
        $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
        session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
        session_name($session_name); // Imposta il nome di sessione con quello prescelto all'inizio della funzione.
        session_start(); // Avvia la sessione php.
        session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
}

function login($input, $password, $mysqli) {
   echo("ciao");
   $email = $input;
   $username = $input;
   if ($stmt = $mysqli->prepare("SELECT UserName, Mail, Password, sale, Organizzatore, Amministratore, Disabilitato 
   FROM utente WHERE (Mail=? OR UserName =?)")) { 
      $stmt->bind_param('ss', $email, $username); // esegue il bind del parametro '$email' e $username.
      $stmt->execute(); // esegue la query appena creata.
      $stmt->store_result();
      $stmt->bind_result($username, $email, $db_password, $sale, $organizzatore, $amministratore, $disabilitato); // recupera il risultato della query e lo memorizza nelle relative variabili.
      $stmt->fetch();
      $password = hash('sha512', $password.$sale);
      if(!$disabilitato){
         if($stmt->num_rows == 1) { // se l'utente esiste
            // verifichiamo che non sia disabilitato in seguito all'esecuzione di troppi tentativi di accesso errati.
            if(checkbrute($username, $mysqli) == true) { 
               // Account disabilitato
               $_SESSION["Disabilitato"]="Sei stato disabilitato";
               $msg = "sei stato disabilitato in seguito a troppi tentativi errati";
               // Invia un e-mail all'utente avvisandolo che il suo account è stato disabilitato.
               sendmail("$email","Disabilitato",$msg);
               return false;
            } else {
                   if($db_password == $password) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
                           // Password corretta!            
                           $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente.
                           $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username); // ci proteggiamo da un attacco XSS
                           $_SESSION['username'] = $username;
                           $_SESSION['mail'] = $email;
                           $_SESSION['login_string'] = hash('sha512', $password.$user_browser);
                           if($organizzatore!=0){
                              $_SESSION["org"] = "ok";
                           }
                           if($amministratore!=0){
                              $_SESSION["admin"] = "ok";
                           }
                           // Login eseguito con successo.
                           return true;    
                   } else {
                           // Password incorretta.
                           // Registriamo il tentativo fallito nel database.
                           $now = time();
                           $mysqli->query("INSERT INTO tentativi_login (FK_UserName, ora) VALUES ('$username', '$now')");
                           return false;
                   }
           }
         } else {
            // L'utente inserito non esiste.
            // echo('<p>non esisti<br/></p>');
            return false;
         }
      }else{
         $_SESSION["disabilitato"]="Sei stato disabilitato";
         return false;
      }
      
   }
}

function checkbrute($username, $mysqli) {
   // Recupero il timestamp
   $now = time();
   // Vengono analizzati tutti i tentativi di login a partire dalle ultime due ore.
   $valid_attempts = $now - (2 * 60 * 60);
   if ($stmt = $mysqli->prepare("SELECT ora FROM tentativi_login WHERE FK_UserName = ? AND ora > '$valid_attempts'")) {
      $stmt->bind_param('s', $username);
      // Eseguo la query creata.
      $stmt->execute();
      $stmt->store_result();
      // Verifico l'esistenza di più di 5 tentativi di login falliti.
      if($stmt->num_rows > 5) {
         echo("sto bloccando ".$username);
         $stmt = $mysqli->prepare("UPDATE utente SET Disabilitato = '1' WHERE `utente`.`UserName` = ?");
         $stmt->bind_param('s', $username);
         $stmt->execute();
         return true;
      } else {
         return false;
      }
   }
}

function login_check($mysqli) {
   // Verifica che tutte le variabili di sessione siano impostate correttamente
   if(isset($_SESSION['username'], $_SESSION['login_string'])) {
     $login_string = $_SESSION['login_string'];
     $username = $_SESSION['username'];     
     $user_browser = $_SERVER['HTTP_USER_AGENT']; // reperisce la stringa 'user-agent' dell'utente.
     if ($stmt = $mysqli->prepare("SELECT Password FROM utente WHERE UserName = ? LIMIT 1")) { 
        $stmt->bind_param('s', $username); // esegue il bind del parametro '$user_id'.
        $stmt->execute(); // Esegue la query creata.
        $stmt->store_result();
 
        if($stmt->num_rows == 1) { // se l'utente esiste
           $stmt->bind_result($password); // recupera le variabili dal risultato ottenuto.
           $stmt->fetch();
           $login_check = hash('sha512', $password.$user_browser);
           if($login_check == $login_string) {
              // Login eseguito!!!!
              return true;
           } else {
              //  Login non eseguito
              return false;
           }
        } else {
            // Login non eseguito
            return false;
        }
     } else {
        // Login non eseguito
        return false;
     }
   } else {
     // Login non eseguito
     return false;
   }
}

function console_log( $data ){
   echo '<script>';
   echo 'console.log('. json_encode( $data ) .')';
   echo '</script>';
 }


function sendmail($account,$subject, $body){

   require("src/PHPMailer.php");
   require("src/SMTP.php");
   $mail = new PHPMailer\PHPMailer\PHPMailer();
   $mail->IsSMTP(); // enable SMTP

   $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
   $mail->SMTPAuth = true; // authentication enabled
   $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
   $mail->Host = "smtp.gmail.com";
   $mail->Port = 465; // or 587
   $mail->IsHTML(true);
   $mail->Username = "mail.event.ino@gmail.com";
   $mail->Password = "Poi0987654321";
   $mail->SetFrom("mail.event.ino@gmail.com");
   $mail->Subject = "$subject";
   $mail->Body = "$body";
   $mail->AddAddress("$account");

   if(!$mail->Send()) {
      echo "Mailer Error: " . $mail->ErrorInfo;
   } else {
      echo "Message has been sent";
   }
}

?>