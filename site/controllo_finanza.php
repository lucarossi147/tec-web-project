<?php
    include 'functions.php';
    sec_session_start();
    require_once("connection.php");
    require 'Encryption.php';
?>

<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Jquery Libraries -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="crypto-js-3.1.9-1/crypto-js.js"></script><!-- https://github.com/brix/crypto-js/releases crypto-js.js can be download from here -->
    <script src="Encryption.js"></script>
    <!-- Ajax lib 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    -->

    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
    crossorigin="anonymous">

    <!-- Jquery-Ui -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Main Css -->
    <link rel="stylesheet" href="css/main.css">

    <script src="control.js"></script>
    <title> Info pagamento | Event.ino </title>
  </head>

<body>
    <?php
        require_once("header.php");
        if(!login_check($conn)){
            //Redirect
            $_SESSION['error']='Accedi per favore';
            header("location: login.php");
            exit();
        }
        
        if(isset($_SESSION['username'])){
            $usr = $_SESSION['username'];
        }
    ?>
<!-- radio dell'indirizzo -->
<?php
    if($stmt= $conn->prepare("SELECT * FROM `spedizione` WHERE FK_Username=? LIMIT 10")){    
        //Bind 
        $stmt->bind_param("s",$usr);
        //Execute
        $stmt->execute();
        //Ottieni result
        $result = $stmt->get_result();
        if($result->num_rows>0){
            echo "<h2> Indirizzi utente: </h2>" . "\n";
            $cont=0;
            while($row = $result->fetch_assoc()) {
                $codice_luogo=$row['FK_Codice_luogo'];
                $codice_spedizione=$row['Codice_spedizione'];
                if($stmt= $conn->prepare("SELECT * FROM `luogo_spedizione` WHERE Codice_luogo=?")){
                    //Bind 
                    $stmt->bind_param("i",$codice_luogo);
                    //Execute
                    $stmt->execute();     
                    //Ottieni result
                    $result2 = $stmt->get_result();
                    while($row2 = $result2->fetch_assoc()) {
                        $codice_luogo = $row2['Codice_luogo'];
                        $interno = $row2['Interno'];
                        $via = $row2['Via'];
                        $civico = $row2['Civico'];
                        $localita = $row2['FK_Localita'];
                        $nome_localita='';
                        if($stmt = $conn->prepare("SELECT `Nome_localita` FROM `localita_` WHERE `Id_localita` = ?")){
    
                            //Bind dei parametri
                            $stmt->bind_param("s", $localita);
                                
                            //Eseguo query
                            $stmt->execute();
                        
                            //Ottengo risultato
                            $result3 = $stmt->get_result();
                            while($row3 = $result3->fetch_assoc()) { 
                                $nome_localita=$row3['Nome_localita'];
                            }
                        }else { 
                            //Errore insert
                            $error = $conn->errno . ' ' . $conn->error;
                            echo $error;
                            exit();
                        }    
                        $predefinito = $row2['predefinito'];
                        echo " <div class='form-check'> ";
                        if(1){
                            echo "<input class='form-check-input' name='selezione_indirizzo' type='radio' value='$codice_luogo' id='defaultCheck$cont' checked>" . "\n";
                        } else {
                            echo "<input class='form-check-input' name='selezione_indirizzo' type='radio' value='$codice_luogo' id='defaultCheck$cont' >" . "\n";
                        }
                            echo "<label class='form-check-label' for='defaultCheck$cont'> $nome_localita -  $via - N. $civico - Interno: $interno</label>" . "\n";                    
                            $cont++;
                        }
                    echo "</div>" . "\n";        
                }
            }

        }else{
            echo "<h2>Aggiungi un indirizzo col form qui sotto!</h2>";
        }
        
    } 
?>
    <button type="button" id="button-ind" class="btn btn-primary" data-toggle="button" aria-pressed="false" >
        Aggiungi un nuovo indirizzo
    </button>

        <form class="needs-validation" id="form_indirizzo" style="display:none" novalidate>
            <div>
                <!-- Selezione Regione --> 
                <div class="col">
                    <label for="form-selected-regione" >Regione</label>
                    <select class="form-control" id="form-selected-regione" name="form-selected-regione" required>
                    <?php
                        $sql = "SELECT DISTINCT `Regione` FROM `localita_` ORDER BY `localita_`.`Regione` ASC";
                        $result = $conn->query($sql);    
                        if ($result->num_rows > 0) {
                            while($row = $result->fetch_assoc()) {
                                $regione=$row['Regione'];
                                echo '<option value="' . $regione . '"> ' . $row["Regione"] . '</option>';
                                //echo '<option value="$rei"> ' . $row["Regione"] . '</option>';
                            }
                        } else {
                            echo "0 results";
                        } 
                    ?>
                    </select>
                </div>

                <!--Insert provincia-->
                <div class="col">
                    <label for="form-selected-provincia">Provincia</label>
                    <select class="form-control" id="form-selected-provincia" name="form-selected-provincia" required>
                        <option value = "">--Provincia--</option> 
                    </select>
                </div>

                <!--Insert localita-->
                <div class="col">
                    <label for="form-selected-localita">Località</label>
                    <select class="form-control" id="form-selected-localita" name="form-selected-localita" required>
                    <option value="">--Località--</option> 
                    </select>
                </div>

                <!--Insert via-->
                <div class="col">
                    <label for="sped-via">Via:</label>
                    <input type="text" class="form-control" id="sped-via" placeholder="via" 
                    name="sped-via" required>
                    <div class="valid-feedback">Valido</div>
                    <div class="invalid-feedback">Inserisci via</div>
                </div>
                
                <!--Insert civico-->
                <div class="col">
                    <label for="sped-civico">Civico:</label>
                    <input type="text" class="form-control" id="sped-civico" placeholder="civico" name="sped-civico" required>
                    <div class="valid-feedback">Valido</div>
                    <div class="invalid-feedback">Inserisci immagine</div>
                </div>

                <!--Insert interno-->
                <div class="col">
                    <label for="sped-interno">Interno:</label>
                    <input value ="" type="text" class="form-control" id="sped-interno" placeholder="interno" autocomplete="off" 
                    name="sped-interno">
                    <input value ="<?php echo $predefinito ?>" type="hidden" class="form-control" id="predefinito" name="predefinito">
                </div>

                <!-- Submit indirizzo -->
                <button formaction='insert_indirizzo.php' 
                        formmethod='POST' type='submit' class='btn btn-primary mb-2'> Inserisci nuovo indirizzo
                </button>
            </div>
        </form>
<?php
    if($stmt= $conn->prepare("SELECT FK_Codice_carta FROM possedimento WHERE FK_UserName=? LIMIT 10")){    
        echo "<h2> Metodi di pagamento: </h2>" ."\n";
        //Bind 
        $stmt->bind_param("s",$usr);
        //Execute
        $stmt->execute();
        //Ottieni result
        $resultpay = $stmt->get_result();
        $cont=0;
        while($rowpay = $resultpay->fetch_assoc()) {
            
            //prendo il numero carta per ogni carta
            $codice_carta=$rowpay['FK_Codice_carta'];
            if($stmt= $conn->prepare("SELECT * FROM `metodo_di_pagamento` WHERE Codice_carta=?")){
                //Bind 
                $stmt->bind_param("i",$codice_carta);
                //Execute
                $stmt->execute();     
                //Ottieni result
                $resultpayint = $stmt->get_result();
                while($rowpayint = $resultpayint->fetch_assoc()) {
                    $Encryption = new Encryption();
                    $scadenza = $rowpayint['Scadenza'];
                    $predefinito = $rowpayint['Predefinito'];
                    $finali = $rowpayint['Finali'];
                    echo "<div class='form-check'>"."\n";
                    if($predefinito==1){
                        echo "<input class='form-check-input' id='pay-selected$cont' name='selezione_pagamento' type='radio' value='$codice_carta' checked>" ."\n";
                    } else {
                        echo "<input class='form-check-input' id='pay-selected$cont' name='selezione_pagamento' type='radio' value='$codice_carta'>"."\n"; 
                    }
                        echo "<label class='form-check-label' for='pay-selected$cont' > **** - **** - **** - *$finali </label> "."\n" ;                    
                    }
                    echo "</div>" . "\n";
                    $cont++;                
            }
        }
    }    
?>

    <button type="button" id="button-pay" class="btn btn-primary" data-toggle="button" aria-pressed="false" >
        Aggiungi un nuovo metodo di pagamento
    </button>

        <form class="needs-validation" id="form_pagamento" style="display:none" novalidate>
            <div>
                <!--Insert numero carta-->
                <div class="col">
                    <label for="pay-carta">Numero carta:</label>
                    <input type="text" class="form-control" id="pay-carta" placeholder="Numero carta" 
                    name="pay-carta" required>
                    <div class="valid-feedback">Valido</div>
                    <div class="invalid-feedback" id="numero-valid">Inserisci numero carta</div>
                </div>
                
                <!--Insert scadenza-->
                <div class="col">
                    <label for="pay-scadenza">Scadenza:</label>
                    <input type="text" class="form-control" id="pay-scadenza" placeholder="MM/AA" name="pay-scadenza" required>
                    <div class="valid-feedback">Valido</div>
                    <div class="invalid-feedback" id="scadenza-valid">Inserisci scadenza</div>
                </div>

                <button 
                    id="button-add-pay" type="button" class='btn btn-primary mb-2'> Inserisci
                </button>
            </div>
        </form>
        <script> // Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<br/>
<a href='process_payment.php'>
<button type="button" id="pas-to-process" class="btn btn-primary" data-toggle="button" aria-pressed="false" >
       paga
    </button>
</a>
<?php
    require_once("footer.html");
?>