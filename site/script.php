<?php
    require_once("connection.php");
    // Nome del file
    $filename = 'citta.csv';
    
    // Percorso da cui prelevare il file
    $path = '';
    
    //Codice, cap, città regione, sigla provincia
    // File completo di percorso
    $file = $filename;
    // Leggo il contenuto del file
    $rows = file( $file );
    
    // Scorro l'array contenente le righe del file
    foreach ( $rows as $row ) {
        // Separo le colonne
        $columns = explode( ',', $row );
        if($stmt= $conn->prepare("INSERT INTO `localita_` (`Nome_localita`, `Provincia`, `Regione`, `CAP`) VALUES (?,?,?,?)")){

            //Bind dei parametri
            $stmt->bind_param("sssi",$columns[1], $columns[3], $columns[2], $columns[0]);
                
            //Eseguo query
            $stmt->execute();

            //Ottengo risultato
            $result = $stmt->get_result();
            $error = $stmt->errno . ' ' . $stmt->error;
            echo $error;
        }else{
            $error = $stmt->errno . ' ' . $stmt->error;
            echo $error;
        }
    }
?>