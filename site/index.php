<?php
  include 'functions.php';
  sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

    <!-- Jquery Libraries -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <!-- Main Css -->
    <link rel="stylesheet" href="css/main.css">
    <title> Home | Event.ino </title>
  </head>

  <body>
    <?php
      require_once("connection.php");

    // Header    
      require_once("header.php");   

    //User Feed   
    if(login_check($conn)){
      if(isset($_SESSION['complimenti'])){
        $bene=$_SESSION['complimenti'];
        echo("<p class='bravo'>$bene</p>");
        unset($_SESSION['complimenti']);
      }
      require_once("user_feed.php");
    }
    //Popular Events
      require_once("popular_events.php");   

    // Before-Footer links
      echo "<div class ='text-center'><a class='before-footer' href='events.php'> Visualizza tutti gli eventi </a></div>";

    // Footer 
      require_once("footer.html");
    ?>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>

</html>