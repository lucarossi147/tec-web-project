$(document).ready(function() {
     $('.datepicker').each(function(){
        $(this).datepicker({
            autoclose: true,
            dateFormat: 'yy-mm-dd',
            todayBtn: "linked"
        });
    }); 

        $("#form-selected-struttura").on('change',function(){  
          var codice_evento = $("#form-selected-struttura").val();
          $.ajax({  
            type: "POST",
            url: "get_info.php",  
            data: "Codice_evento=" + codice_evento,
            dataType: "html",
            success: function(risposta) {
                var array = $.parseJSON(risposta);
                var indirizzo_struttura = array.indirizzo_struttura;
                var nome_struttura = array.nome_struttura;
                var codice_manifestazione = array.codice_manifestazione;
                var nome_evento = array.nome_evento;
                var img_evento = array.img_evento;
                var descrizione = array.descrizione;
                var num_ospiti = array.num_ospiti;
                var posti_disponibili = array.posti_disponibili;
                var biglietti = array.biglietti;
                var data_evento = array.data_evento;
                var categoria_evento = array.categoria_evento;
                $("input#event_name").attr({
                    "value":nome_evento,
                });
                $("input#event_image").attr({
                    "value":img_evento,
                });
                
                $("textarea#descrizione_evento").html(descrizione);

                $("input#posti_evento").attr({
                    "value":posti_disponibili,
                });
                $("input#biglietti_evento").attr({
                    "value":biglietti,
                });

                $('input#form-selected-tipo option[value=codice_evento]').attr('selected','selected');

                $('input#form-selected-struttura option[value=codice_evento]').attr('selected','selected');

                $('input#form-selected-manifest option[value=codice_evento]').attr('selected','selected');

                $("input#num_ospiti").attr({
                    "value":num_ospiti,
                });
                $("input#datepicker").attr({
                    "value":data_evento,
                });
            },
            error: function(){
              alert("Chiamata fallita!!!");
            } 
          }); 
          return false;  
        });

        $("#form-selected-regione").on('change',function(){  
            var regione = $("#form-selected-regione").val();
            $.ajax({  
              type: "POST",
              url: "select-localita.php",  
              data: "regione=" + regione,
              dataType: "html",
              success: function(risposta) {
                  $("#form-selected-provincia").empty();
                  $("#form-selected-localita").empty();
                  var array = $.parseJSON(risposta);
                  var optionLabel = $("<option></option>").text("");
                    $("#form-selected-provincia").append(optionLabel);
                  array.forEach(element => {
                    optionLabel = $("<option></option>").text(element.Provincia);
                    $("#form-selected-provincia").append(optionLabel);
                  });
              },
              error: function(){
                alert("Chiamata fallita!!!");
              } 
            }); 
            return false;  
        });
        
        $("#form-selected-provincia").on('change',function(){  
          var provincia = $("#form-selected-provincia").val();
          $.ajax({  
            type: "POST",
            url: "select-localita.php",  
            data: "provincia=" + provincia,
            dataType: "html",
            success: function(risposta) {
                $("#form-selected-localita").empty();
                var optionLabel = $("<option></option>").text("");
                $("#form-selected-localita").append(optionLabel);
                var array = $.parseJSON(risposta);
                array.forEach(element => {
                  var optionLabel = $("<option></option>").text(element.Nome_localita);
                  $("#form-selected-localita").append(optionLabel);
                });
            },
            error: function(){
              alert("Chiamata fallita!!!");
            } 
          }); 
          return false;  
      });

      $("#button-ind").click(function(){
        if($("#form_indirizzo").is(":visible")){
          $("#form_indirizzo").hide();
        }else{
          $("#form_indirizzo").show();
        } 
      });

      $("#button-pay").click(function(){
        if($("#form_pagamento").is(":visible")){
          $("#form_pagamento").hide();
        }else{
          $("#form_pagamento").show();
        } 
      });

      function dec2hex (dec) {
        return ('0' + dec.toString(16)).substr(-2)
      }
      
      // generateId :: Integer -> String
      function generateId (len) {
        var arr = new Uint8Array((len || 40) / 2)
        window.crypto.getRandomValues(arr)
        return Array.from(arr, dec2hex).join('')
      }

      $("#button-add-pay").click(function(){  
        var numero_carta = $("#pay-carta").val();
        var scadenza = $("#pay-scadenza").val();
        var len_carta = numero_carta.length;
        var len_scadenza = scadenza.length;

        if(len_scadenza!=5){
          $("#scadenza-valid").html('Inserisci scadenza valida');
          $("#scadenza-valid").show();
        }else{
          $("#scadenza-valid").hide();
        }
        
        if(len_carta!=16){
          $("#numero-valid").html('Inserisci numero carta valido');
          $("#numero-valid").show();
        }else{
          $("#numero-valid").hide();
        }

        if(len_carta == 16 && len_scadenza == 5){
          var finali = numero_carta.substr(13,15);
          var nonce = generateId(32);
          let encryption = new Encryption();
          var encrypted_carta = encryption.encrypt(numero_carta, nonce);
          var encrypted_scadenza = encryption.encrypt(scadenza, nonce);
          $("#pay-carta").val('') ;
          $("#pay-scadenza").val('');
          $.ajax({  
            type: "POST",
            url: "add_pay.php",  
            data: "carta=" + encrypted_carta+"&scadenza="+encrypted_scadenza+"&nonce="+nonce+"&finali="+finali,
            dataType: "html",
            success: function(risposta) {  
            },
            error: function(){
              alert("Chiamata fallita!!!");
            } 
          });
          return false;  
        }
      });

      //promuovi ad organizzatore
      $("#btn-become-org").click(function(){  
        var username = $("#become_organizer").val();
        $("#become_organizer").val('');
        $.ajax({  
          type: "POST",
          url: "promove.php",  
          data: "utente_upgrade=" + username,
          dataType: "html",
          success: function(risposta) {  
          },
          error: function(){
            alert("Chiamata fallita!!!");
          } 
        });
      return false;  
  
      });

      /*
        Elimina vecchie ricevute
      */
      $("#ticket_button").click(function(){  
        $.ajax({  
          type: "POST",
          url: "remove_ticket.php",  
          data: "",
          dataType: "html",
          success: function(risposta) {  
            $("#ticket h2").css("color","green");
            $("#ticket h2").html("Eliminazione avvenuta");
          },
          error: function(){
            alert("Chiamata fallita!!!");
          } 
        });
      return false;
      });

});