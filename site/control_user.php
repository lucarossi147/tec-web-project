<?php
    include 'functions.php';
    require_once("connection.php");
    sec_session_start();
    
    if(isset($_SESSION['username'])){
        $user=$_SESSION['username'];
    }
    

    $org=0;
    $amm=0;

    /**
     * Controllo i privilegi di chi sta aggiungendo l'evento
     */
    if($stmt= $conn->prepare("SELECT Organizzatore,Amministratore FROM utente WHERE UserName=?")){

        //Bind 
        $stmt->bind_param("s",$user);

        //Execute
        $stmt->execute();

        //Ottieni result
        $result = $stmt->get_result();

        //Estraggo risultato -> organizz. e amministratore
        $row = $result->fetch_array(MYSQLI_NUM);

        //Creo array di valori per la situazione utente
        $diritti=array();

        $org = $row[0];
        $amm = $row[1];

        //Riempo l'array
        array_push($diritti,$org,$amm);

    }else{
        exit();
    }

    //Encone in json e rispondo allo script
    echo json_encode($diritti);  
?>
