<?php
  include 'functions.php';
  sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Jquery Libraries -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

    <!-- Ajax lib 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    -->
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
    crossorigin="anonymous">

    <!-- Jquery-Ui -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Main Css -->
    <link rel="stylesheet" href="css/main.css">

    <script src="control.js"></script>
    <script src="verify.js"></script>
    
    <title> Nuova manifestazione | Event.ino </title>

  </head>

  <body>
    <?php
        // Header    
        require_once("header.php");   
        require_once("connection.php");
    ?>
    <h3 class="title"> Aggiungi manifestazione </h3>
    <form class="form-area needs-validation" novalidate >
        <div class="form-row">
            <div class="col">
                <input title="write manifest name" type="text" value="" id="name-man" name="name-man" 
                class="form-control form-x" autocomplete ="off" placeholder="Nome manifestazione" required>
                <div class="valid-feedback"> Valido </div>
                <div class="invalid-feedback">Perfavore, inserisci il nome della manifestazione</div>
            </div>
            <div class="col">
                <input title="write manifest events" type="number" value="" id="num-man" name="num-man" 
                class="form-control form-x" placeholder="Numero eventi" required>
                <div class="valid-feedback"> Valido </div>
                <div class="invalid-feedback">Perfavore, inserisci il numero di eventi</div>
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <label for="image-man" class="label-text-area">Percorso immagine:</label>
                    <input type="text" value="" name="image-man" 
                    class="form-control form-x" id="image-man" placeholder="images/{...}" required>
                    <div class="valid-feedback"> Valido </div>
                    <div class="invalid-feedback">Perfavore, inserisci il percorso dell'immagine</div>
            </div>
        </div>
        <div class="form-group">
            <label class="label-text-area" for="desc-man">Descrizione manifestazione</label>
            <textarea class="form-control form-x" id="desc-man" rows="5" 
            name="desc-man" placeholder="Inserisci descrizione manifestazione" required></textarea>
            <div class="valid-feedback"> Valido </div>
            <div class="invalid-feedback">Perfavore, inserisci la descrizione</div>
        </div>
        <!-- INSERIMENTO DATA -->
        <div class="form-row">
            <div class=col>
                <label for="datepickerstart" class="date-label">Seleziona data inizio:</label>   
                    <input name="datepickerstart" id="datepickerstart" class="form-control input-sm datepicker form-x" required>
                <div class="valid-feedback"> Valido </div>
                <div class="invalid-feedback">Perfavore, inserisci la data di inzio</div>
            </div>
            <div class=col>
                <label for="datepickerfinish" class="date-label">Seleziona data fine: </label> 
                    <input  name="datepickerfinish"  id="datepickerfinish" class="form-control input-sm datepicker form-x" required>
                <div class="error-row" id="error"></div> 
                <div class="valid-feedback"> Valido </div>
                <div class="invalid-feedback">Perfavore, inserisci la data di fine</div>
            </div>
            <div> 
              <label id="disable" for="disable_button"></label>
                <button id="disable_button" formaction="adding_script.php" 
                formmethod="POST" type="submit" class="btn btn-primary mb-2">Inserisci</button>
            </div>
        </div>
        <!-- FINE INSERIMENTO DATA -->
    </form>

    <?php
      echo "<a href='insert_structure.php'>
              <button type='button' class='btn btn-primary'>NEXT</button>
            </a>";
    ?>
    <script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>  
    <?php  require_once("footer.html");     ?>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body> 

</html>