<?php
include 'functions.php';
sec_session_start();
require_once("connection.php");

if(isset($_POST['form-selected-localita'])){
    $localita=$_POST['form-selected-localita'];
}

if(isset($_SESSION['username'])){
    $usr=$_SESSION['username'];
}

$interno=$_POST['sped-interno'];
$via=$_POST['sped-via'];
$civico=$_POST['sped-civico'];
$predefinito=$_POST['predefinito'];
$result=NULL;

if($stmt= $conn->prepare("SELECT `Id_localita` FROM `localita_` WHERE `Nome_localita`=?")){
    
    //Bind dei parametri
    $stmt->bind_param("s", $localita);
        
    //Eseguo query
    $stmt->execute();

    //Ottengo risultato
    $result = $stmt->get_result();
    while($row = $result->fetch_assoc()) { 
        $codice_localita=$row['Id_localita'];
    }
    $error = $stmt->errno . ' ' . $stmt->error;
    echo $error;
}else { 
    //Errore insert
    $error = $conn->errno . ' ' . $conn->error;
    echo $error;
    exit();
}


if($stmt = $conn->prepare("INSERT INTO `luogo_spedizione`(`Interno`, 
    `Via`, `Civico`, `FK_Localita`, `predefinito`) VALUES (?,?,?,?,?)")){
    
    //Bind dei parametri
    $stmt->bind_param("isiii", $interno, $via, $civico,$codice_localita, $predefinito);
            
    //Eseguo query
    $stmt->execute();

    //Ottengo risultato
    $result = $stmt->insert_id;
        
    $error = $stmt->errno . ' ' . $stmt->error;
    echo $error;

    } else { //Errore insert
        $error = $conn->errno . ' ' . $conn->error;
        echo $error;
        exit();
    }
    
    if($stmt= $conn->prepare("INSERT INTO `spedizione`(`FK_Codice_luogo`, `Codice_spedizione`, 
    `FK_UserName`) VALUES (?,NULL,?)")){
        
    //Bind dei parametri
    $stmt->bind_param("is", $result, $usr);
            
    //Eseguo query
    $stmt->execute();    
        
    $error = $stmt->errno . ' ' . $stmt->error;
    echo $error;
    
    } else { //Errore insert
        $error = $conn->errno . ' ' . $conn->error;
        echo $error;
        exit();
    }
    //Redirect
    header("location: controllo_finanza.php");
?>