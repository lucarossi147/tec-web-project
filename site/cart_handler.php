<?php
    include 'functions.php';
    sec_session_start();
    
    require_once("connection.php");

    $mode = $_GET["mode"];
    $product_id = $_GET["product_id"];


    if(login_check($conn)){
        //Get user from session
        $user = $_SESSION["username"];

        $acquisto;
        $codice_acquisto;

        //Check if exists a singolo acquisto not confirmed, if not, creates one.
        $sql = "SELECT * FROM `acquisto` WHERE (Data_acquisto IS NULL)
                                                        AND (FK_UserName=?)";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param('s', $user);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        echo "$result->num_rows";
        if($result->num_rows==0){
            $sql2 = "INSERT INTO `acquisto`(FK_UserName) VALUES(?)";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->bind_param('s', $user);
            $stmt2->execute();
            $stmt2 ->close();
            $sql3 = "SELECT Codice_acquisto FROM `acquisto` WHERE (Data_acquisto IS NULL)
                                                AND (FK_UserName=?)";
            $stmt3 = $conn->prepare($sql3);
            $stmt3->bind_param('s', $user);
            $stmt3->execute();
            $result = $stmt3->get_result();
            $stmt3 ->close();
            $acquisto = $result->fetch_assoc();
            $codice_acquisto = $acquisto["Codice_acquisto"];
            echo "$codice_acquisto";
        } else {
            $acquisto = $result->fetch_assoc();
            $codice_acquisto = $acquisto["Codice_acquisto"];
        }
        if($mode=='a'){
            $sql = $conn->prepare("INSERT INTO `singolo_acquisto` (FK_Codice_acquisto, FK_Codice_prodotto) 
                                                VALUES('$codice_acquisto', '$product_id')");
            $sql->execute();
            $sql->close();
            $sql2 = $conn->prepare("UPDATE `prodotto` SET `Nel_carrello` = '1' WHERE `prodotto`.`Codice_prodotto` = '$product_id'");
            $sql2->execute();
            $sql2->close();
        } else if($mode=='r'){
            $sql = $conn->prepare("DELETE FROM `singolo_acquisto` WHERE (FK_Codice_acquisto='$codice_acquisto')
                                                                    AND (FK_Codice_prodotto='$product_id')");
            $sql->execute();
            $sql->close();
            $sql2 = $conn->prepare("UPDATE `prodotto` SET `Nel_carrello` = '0' WHERE `prodotto`.`Codice_prodotto` = '$product_id'");
            $sql2->execute();
            $sql2->close();
        }
    }
?>