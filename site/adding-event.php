<?php
include 'functions.php';
sec_session_start();

    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    require_once("connection.php");
    if (isset($_POST)){

        $nome_struttura = htmlentities($_POST["nome_struttura"],ENT_QUOTES);
        $nome_manifestazione = htmlentities($_POST["nome_manifestazione"],ENT_QUOTES);
        $event_name = htmlentities($_POST["event_name"],ENT_QUOTES);
        $img_evento = htmlentities($_POST["event_image"],ENT_QUOTES);
        $descrizione = htmlentities($_POST["event_desc"],ENT_QUOTES);
        $num_ospiti = htmlentities($_POST["num_ospiti"],ENT_QUOTES);
        $posti_disponibili = htmlentities($_POST["posti_evento"],ENT_QUOTES);
        $biglietti_venduti = htmlentities($_POST["biglietti_evento"],ENT_QUOTES);
        $categoria_evento = htmlentities($_POST["categoria_evento"],ENT_QUOTES);
        $data_evento = htmlentities($_POST["data_evento"],ENT_QUOTES);

        //Recupero username utente loggato
        $user = $_SESSION['username'];

        $codice_manifestazione=0;
        /**
         * Selezione indirizzo a partire dalla struttura
         */
        if($stmt= $conn->prepare("SELECT Indirizzo_struttura FROM struttura WHERE Nome_struttura=?")){

            //Bind 
            $stmt->bind_param("s",$nome_struttura);

            //Execute
            $stmt->execute();

            //Ottieni result
            $result = $stmt->get_result();

            //Estraggo risultato -> indirizzo struttura
            $row = $result->fetch_array(MYSQLI_NUM);
            $indirizzo_struttura = $row[0];
        }

        /**
         * Selezione codice manifestazione a partire dal nome della manifestazione
         */
        if($stmt= $conn->prepare("SELECT Codice_manifestazione FROM manifestazione WHERE Nome_manifestazione=?")){

            //Bind 
            $stmt->bind_param("s",$nome_manifestazione);
            
            //Execute
            $stmt->execute();

            //Ottieni result
            $result = $stmt->get_result();

            //Estraggo risultato -> codice manifestazione
            $row = $result->fetch_array(MYSQLI_NUM);
            $codice_manifestazione = $row[0];
        }

        $posti_disponibili=0;
        //Creo statement per inserire una nuova manifestazione e controllo        
        if($stmt= $conn->prepare("INSERT INTO `evento` (`Codice_evento`, `FK_Indirizzo_struttura`,
        `FK_Nome_struttura`, `FK_Codice_manifestazione`, `Nome_evento`, `Img_evento`, `Descrizione`, 
        `Num_ospiti`,`Posti_disponibili`,`Biglietti_venduti`,`Data_evento`,`FK_Nome_categoria_evento`) 
        VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?)")){

            //Bind dei parametri
            $stmt->bind_param("ssisssiiiss", $indirizzo_struttura, $nome_struttura, $codice_manifestazione, 
            $event_name, $img_evento, $descrizione, $num_ospiti, $posti_disponibili, 
            $biglietti_venduti, $data_evento, $categoria_evento);
                
            //Eseguo query
            $stmt->execute();

            //Ottengo risultato
            $result = $stmt->get_result();
            
            $codice_evento=$stmt->insert_id;

        }
        else { 
            //Errore insert
            $error = $conn->errno . ' ' . $conn->error;
            echo $error;
            echo "<p>Errore nello script, torna alla pagina di creazione</p>";
            echo "<a href='insert_event.php'>Torna</p>";
            exit();
        }
        $i=0;
        $posti_disponibili=100;
        for($i=0;$i<$posti_disponibili;$i++){
            console_log("A");
            $stmt = $conn->prepare("INSERT INTO `prodotto` (`Nome`, `FK_Codice_categoria_prodotto`,
            `Importo`,`Descrizione`, `FK_Codice_manifestazione`,`FK_Codice_evento`,`Nel_carrello`) 
            VALUES (?,?,?,?,?,?,?)");
                
                if($stmt= $conn->prepare("SELECT Mail FROM utente WHERE UserName=?")){
                    //Bind 
                    $stmt->bind_param("s",$user);

                    //Execute
                    $stmt->execute();

                    //Ottieni result
                    $result = $stmt->get_result();

                    $row = $result->fetch_array(MYSQLI_NUM);
                    $mail = $row[0];

                    //Creo messagggio
                    $message = "L'evento' è stata aggiunta";
                    $message = wordwrap($message, 70, "\r\n");

                    //Invio mail (mail, object of mail, message)
                    //Funziona solo se è settato un mail server
                sendmail($mail, 'Aggiunto evento', $message);
                }
                else {  //Errore select mail
                        //Se avviene un errore, stampo il codice e stampo conn per redirect
                        $error = $conn->errno . ' ' . $conn->error;
                        echo $error;
                        echo "<p>Errore nello script, torna alla pagina di creazione</p>";
                        echo "<a href='insert_event.php'>Torna</p>";
                }
            /* }else { //Errore insert
                $error = $conn->errno . ' ' . $conn->error;
                echo $error;
                echo "<p>Errore nello script, torna alla pagina di creazione</p>";
                echo "<a href='insert_event.php'>Torna</p>";
                exit();
            }*/
            //Redirect
            header("location: insert_event.php");
}
    }
?>