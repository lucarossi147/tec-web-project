function modifyCart(product_id, index){
    var mode;
    var xhttp = new XMLHttpRequest();
    mode = toCart(index);
    xhttp.open("GET", "cart_handler.php?mode="+mode+"&product_id="+product_id, true);
    xhttp.send();
}

function toCart(index){
    var btn = document.getElementById("cart_btn "+index);
    if(btn.innerHTML == "+"){
        btn.innerHTML = "-";
        return 'a';
    } else if(btn.innerHTML == "-"){
        btn.innerHTML = "+";
        return 'r';
    } else if(btn.innerHTML == "X"){
        document.getElementById("cart-el "+index).remove();
        return 'r';
    }
}