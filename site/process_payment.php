<?php
include 'functions.php';
sec_session_start();
require_once("connection.php");
require 'Encryption.php';
if (!login_check($conn)){
    $_SESSION['error']='Sembra tu non sia loggato, riaccedi per favore';
    header("Location:login.php");
    exit;
}
$utente=$_SESSION['username'];

if (isset($_POST['codice_carta'])&& isset($_POST['codice_luogo'])){
    $codice_carta=$_POST['codice_carta'];
    $codice_luogo=$_POST['codice_luogo'];
    $num_biglietti;
    $totale_carrello = 0;
    $msg_mail;

    if ($stmt = $conn->prepare("SELECT * FROM `singolo_acquisto` WHERE FK_Codice_acquisto IN (
                                        SELECT Codice_acquisto FROM `acquisto` WHERE FK_UserName=? AND Data_acquisto IS NULL)")) { 
        $stmt->bind_param('s', $utente); 
        $stmt->execute();
        $result=$stmt->get_result();
        //qui ho il NUMERO DI BIGLIETTI NEL CARRELLO
        $num_biglietti = $result->num_rows; 
        $num_biglietti++;
        $msg_mail="Hai acquistato ".$num_biglietti." biglietti.\r\n";
        while ($row = $result->fetch_assoc()){
            //qui ho il CODICE ACQUISTO E PRODOTTO
            $codice_acquisto=$row['FK_Codice_acquisto'];
            $codice_prodotto=$row['FK_Codice_Prodotto'];
            if($stmt = $conn->prepare("SELECT FK_Codice_evento, Importo FROM prodotto WHERE Codice_prodotto = ?")){
                $stmt->bind_param('s', $codice_prodotto); 
                $stmt->execute();
                $stmt->store_result();
                //qui ho il CODICE EVENTO
                $stmt->bind_result($codice_evento, $importo);
                $stmt->fetch();
                //aggiorno il totale dei biglietti nel carrello
                $totale_carrello+=$importo;
                if($stmt = $conn->prepare("SELECT Nome_evento, Posti_disponibili, Biglietti_venduti, Data_evento, FK_Codice_manifestazione FROM evento WHERE Codice_evento = ?")){
                    $stmt->bind_param('s', $codice_evento); 
                    $stmt->execute();
                    $stmt->store_result();
                    //qui ho il LE INFO SULL' EVENTO
                    $stmt->bind_result($nome_evento,$posti_disp, $biglietti_venduti, $data, $codice_manifestazione);
                    $stmt->fetch();
                    $totale_carrello+=$importo;
                    //sto vendendo un biglietto in piu', quindi controllo che 
                    $biglietti_venduti++;
                    if ($biglietti_venduti<=$posti_disp){
                        if($stmt = $conn->prepare("UPDATE evento SET Biglietti_venduti = ? WHERE evento.Codice_evento = ? ")){
                            $stmt->bind_param('ii',$biglietti_venduti, $codice_evento); 
                            $stmt->execute();
                        }
                        //se non e' cosi vuol dire che gli eventi sono esauriti
                    }else{
                        if($stmt = $conn->prepare("SELECT FK_UserName FROM manifestazone WHERE Codice_manifestazione = ?")){
                            $stmt->bind_param('i',$codice_manifestazione); 
                            $stmt->execute();
                            $stmt->store_result();
                            //qui ho il LE INFO SUL creatore della manifestazione
                            $stmt->bind_result($creator);
                            $stmt->fetch();
                            if($stmt = $conn->prepare("SELECT Mail FROM utente WHERE UserName = ?")){
                                $stmt->bind_param('i',$creator); 
                                $stmt->execute();
                                $stmt->store_result();
                                $stmt->bind_result($cre_mail);
                                $stmt->fetch();
                                sendmail($cre_mail,"Graandee", "Il tuo evento è andato sold out!!");
                            }
                        }
                    }
                    $msg_mail.="Hai comprato un biglietto per l'evento ".$nome_evento." in data ".$data.", il codice del tuo biglietto è ".$codice_prodotto." e il codice dell'evento e' ".$codice_evento."\r\n";
                }
            }
        }
    }
    //effettuo il "pagamento"
    //faccio il fetch delle info di pagamento, faccio inserire all'utente il cvv e pace
    if($stmt = $conn->prepare("SELECT Numero_carta, Chiave FROM metodo_di_pagamento WHERE Codice_carta=?")){
        $stmt->bind_param('i',$codice_carta); 
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($numero_carta_encrypt, $chiave);
        $stmt->fetch();
    }
    $encryption = new Encryption();
    $numero_carta_dec = $encryption->decrypt($numero_carta_encrypt, $chiave);
    //qui la passerei all'eventuale addetto al pagamento

    $data_pagamento = date("d/m/Y");
    $ora_pagamento = date("h:i:sa");
    $imponibile = $totale_carrello*TASSE;
    $mod_pagamento = 'carta di credito';

    if($stmt = $conn->prepare("UPDATE acquisto SET Data_acquisto = ? WHERE Codice_acquisto = ?")){
        $stmt->bind_param('si', $data_pagamento, $codice_acquisto ); 
        $stmt->execute();
    }
    if($stmt = $conn->prepare("INSERT INTO ricevuta(FK_UserName, Data_acquisto, Ora, Prezzo_tot, Qta_Biglietti, Imponibile) VALUES (?,?,?,?,?,?,?,?)")){
        $stmt->bind_param('sssiii', $utente, $data_pagamento, $ora_pagamento, $totale_carrello, $num_biglietti, $imponibile); 
        $stmt->execute();
        $mail=$_SESSION["mail"];
        $msg_mail = wordwrap($msg_mail,70);
        sendmail($mail,"Ricevuta pagamento",$msg_mail);
    }
}

?>
<!DOCTYPE html>
<html lang="it">
<head>
	<title>Process payment - Event.ino</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">	
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<!-- form Css -->
	<link rel="stylesheet" href="css/form.css">
</head>
<body>
<form class="form-container-log">
  <div class="form-group">
    <label id="prezzo"><?php $totale_carrello ?></label>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Immetti il cvv per acquistare:</label>
    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="CVV">
  </div>
  <!-- <a href='pagamento.php'> -->
  <button id="genera-ricevuta" type="button" class="btn btn-primary">paga</button>
  <!--  </a>-->
</form>

</body>
</html>