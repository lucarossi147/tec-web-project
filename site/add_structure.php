<?php
include 'functions.php';
sec_session_start();
require_once("connection.php");

if(isset($_POST['form-selected-localita'])){
    $localita=$_POST['form-selected-localita'];
}

if($stmt= $conn->prepare("SELECT `Id_localita` FROM `localita_` WHERE `Nome_localita`=?")){

    //Bind 
    $stmt->bind_param("s",$localita);

    //Execute
    $stmt->execute();

    //Ottieni result
    $result = $stmt->get_result();

    //Estraggo risultato -> organizz. e amministratore
    $row = $result->fetch_array(MYSQLI_NUM);
    print_r($row);
    $chiave_localita=$row[0];
}else { //Errore select
    $error = $conn->errno . ' ' . $conn->error;
    echo $error;
    exit();
}

if($stmt= $conn->prepare("INSERT INTO `struttura`(`Indirizzo_struttura`, `Nome_struttura`, 
    `FK_Localita`, `Img_struttura`, `Capienza`, `Posti_disponibili`) VALUES (?,?,?,?,?,?)")){
        
        //Bind dei parametri
        $stmt->bind_param("ssisii", $_POST['struct-indirizzo'], $_POST['struct-name'], $chiave_localita,
        $_POST['struct-img'], $_POST['struct-capienza'], $_POST['posti-disponibili']);
            
        //Eseguo query
        $stmt->execute();

        //Ottengo risultato
        $result = $stmt->get_result();

        printf("Errorcode: %d\n", $conn->errno);
        printf("Error: %s\n", $conn->error);
        $mia_mail = $_SESSION['mail'];
        sendmail($mia_mail, "Struttura aggiunta con successo", "La tua struttura e' stata aggiunta correttamente");
    }else { //Errore insert
        $error = $conn->errno . ' ' . $conn->error;
        echo $error;
        exit();
    }
    //Redirect
    header("location: insert_structure.php");
?>