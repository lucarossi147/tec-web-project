<?php
  include 'functions.php';
  sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Jquery Libraries -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Ajax lib 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    -->
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
    crossorigin="anonymous">

    <!-- Jquery-Ui -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Main Css -->
    <link rel="stylesheet" href="css/main.css">

    <script src="control.js"></script>
    <title> Modifica evento | Event.ino </title>

  </head>

  <body>
    <?php
        // Header    
        require_once("header.php");   

        //Db connection
        require_once("connection.php");
    ?>

    <h3 class="title">Modifica evento</h3>

    <form class="needs-validation" novalidate>
    <!-- First, seleziona evento da modificare -->
    <div class="col">

                <?php
                if(isset($_SESSION['username'])){
                    $username=$_SESSION['username'];
                    $sql = "SELECT * FROM evento WHERE FK_Codice_manifestazione IN (SELECT Codice_manifestazione 
                    FROM manifestazione WHERE FK_UserName='$username')";
                /*    
                Old query
                $sql = "SELECT Codice_evento, Nome_evento FROM evento WHERE organizzatore=";
                */    
                $result = $conn->query($sql);    
                    if ($result->num_rows > 0) {
                        echo " <label> Evento da modificare: </label>
                        <select class='form-control' id='form-selected-struttura' name='codice_evento'> ";
                        while($row = $result->fetch_assoc()) { 
                            $codice_evento = $row['Codice_evento'];
                            $nome_evento = $row['Nome_evento'];
                            echo '<option>' . $codice_evento . ' - ' . $nome_evento. '</option>';
                            }
                        require_once("modify_form.php");
                    } else {
                        trigger_error('Invalid query: ' . $conn->error);
                        echo "<div> 
                        Non hai ancora creato un evento, creane uno:
                        <a href='add-event.php'> Clicca qui</a>    
                        </div>";
                    }
                }
                else{
                    echo "<a> Effettua prima il login</a>
                        <a href='login.php'> Clicca qui </a>";
                }
                                        
                ?>
                </select>
            </div>

<script> // Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<?php  
    require_once("footer.html"); 
    $conn->close();
?>  

 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body> 

</html>