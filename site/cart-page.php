<?php
  include 'functions.php';
  sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <!-- Main Css -->
    <link rel="stylesheet" href="css/main.css">
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

    <script src="cart_btn.js"></script>
    <title> Carrello | Event.ino </title>
  </head>

  <body>
    <?php
      
      require_once("connection.php");
      $importo_totale;
      $sub_totale;

      // Header    
      require_once("header.php");   

      $user = $_SESSION["username"];
        
      $sql = $conn->prepare("SELECT * FROM `singolo_acquisto` WHERE FK_Codice_acquisto IN (SELECT Codice_acquisto 
                                                                            FROM `acquisto`
                                                                            WHERE FK_UserName=? AND Data_acquisto IS NULL)");
      $sql->bind_param('s', $user);
      $sql->execute();
      $acquisti = $sql->get_result();
      $sql->close();
      echo "<h1>Il tuo carrello</h1>
            <div class='container'>";

      if($acquisti->num_rows > 0){
        for($i=0;$i<$acquisti->num_rows;$i++){
          $singolo_acquisto = $acquisti->fetch_assoc();
          $codice_prodotto = $singolo_acquisto["FK_Codice_prodotto"];
          $sql_prodotto = $conn->prepare("SELECT * FROM `prodotto` WHERE Codice_prodotto='$codice_prodotto'");
          $sql_prodotto->execute();
          $prodotto = $sql_prodotto->get_result()->fetch_assoc();
          $sql_prodotto->close();
          $name = $prodotto["Nome"];
          $price = $prodotto["Importo"];
          echo "<div class='row' id='cart-el $i'>
                  <div class='col'>$name</div>
                  <div class='col'>$price$</div>
                  <div class='col'>
                    <button type='button' class='btn btn-primary' onclick='modifyCart(`$codice_prodotto`,`$i`)' id='cart_btn $i'>X</button>
                  </div>
                </div>";
        }
        echo "  <a href='controllo_finanza.php'>
                  <button type='button' class='btn btn-primary'>Checkout</button>
                </a>
            </div>";
      } else {
        echo "Il tuo carrello è vuoto al momento </div>";
      }

    // Footer 
      require_once("footer.html");
    ?>
    
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script>
    $(document).ready(function(){            
            $('footer').attr("class", "fixed-bottom");
    });
  </script> 
  </body>

</html>