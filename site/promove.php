<?php
include('functions.php');
require_once('connection.php');

if(isset($_POST['utente_upgrade'])){
    $utente_upgrade=$_POST['utente_upgrade'];
    if($stmt = $conn->prepare("UPDATE utente SET Organizzatore = '1' WHERE utente.UserName = ?")){
        $stmt->bind_param('s', $utente_upgrade); 
        $stmt->execute();
        if($stmt = $conn->prepare("SELECT Mail FROM `utente` WHERE UserName =?")){
            $stmt->bind_param('s', $utente_upgrade); 
            $stmt->execute(); 
            $stmt->store_result();
            $stmt->bind_result($mail);
            $stmt->fetch(); 
            $msg="Complimenti, ora sei un organizzatore, potrai creare i tuoi eventi e manifestazioni.";
            $msg = wordwrap($msg,70);
            sendmail($mail,"Sei stato promosso ad organizzatore", $msg);       
        }

    }
}
    
?>