<?php
  include 'functions.php';
  sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="control.js"></script>
    <!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
  <link rel="stylesheet" type="text/css" href="css/main.css">
  
    <title>Profilo</title>
  </head>
  
  <body>

    <?php
      require_once("connection.php");
      require_once("navbar.php");

      $user = $_SESSION["username"];

      if(login_check($conn)){
        //print_r($_SESSION);
        require_once("user_feed.php");
      }

      if(isset($_SESSION["admin"])){
        require_once('add_organizer.html');
      }

      if(isset($_SESSION["org"])){

        echo "<div id='ticket'>
        <h2>Rimuovi ricevute</h2>
        <button type='button' id='ticket_button'>Change Content</button>
        </div>";
        //Div con gli eventi dell'user
        $sql = $conn->prepare("SELECT * FROM `evento` WHERE FK_Codice_manifestazione IN (SELECT Codice_manifestazione
                                                                          FROM `manifestazione`
                                                                          WHERE FK_UserName = '$user')");
        $sql->execute();
        $events = $sql->get_result();
        $sql->close();
        echo "<h1>I tuoi Eventi</h1>";
        if($events->num_rows > 0){
          for($i=0;$i<$events->num_rows;$i++){
            $event = $events->fetch_assoc();
            $name = $event['Nome_evento'];
            $desc = $event['Descrizione'];
            $event_id = $event['Codice_evento'];
            $image = $event["Img_evento"];
            echo "<div class='card'> 
                    <a href='event-page.php' event=$event_id class='card-link'>
                      <img class='carousel-img card-img-top img-fluid' src='$image' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>    
                    </a>
                    <div class='card-body'>
                      <h5 class='card-title'> $name </h5>
                      <p class='card-text'> $desc </p>
                      <a href='event-page.php?event=$event_id' class='card-link'>INFO</a>
                    </div>
                  </div>";
          }
        }

          echo "<a href='insert_manifest.php'>
                  <button type='button' class='btn btn-primary'>+</button>
                </a>";

          echo "<a href='insert_event.php'> Crea un evento qui <a>
          </div>";

        //Link alla creazione di nuovi eventi
        echo "<div><a href='insert_structure.php'> Crea una struttura qui <a></div>
          </div>";

          echo "<div><a href='insert_manifest.php'> Se non hai ancora creato una manifestazione che contenga un evento creala qui <a></div>
          </div>";
      }

    


    require_once("footer.html");
    ?>
   </body>
</html>