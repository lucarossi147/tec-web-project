<?php
include 'functions.php';
sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

    <!-- Main Css -->
    <link rel="stylesheet" href="css/main.css">

    <script src="follow_btn_handler.js"></script>
    <title> Home | Event.ino </title>
  </head>
  <!-- 
        Header 
    -->
    <?php
      require_once("header.php");
    ?>
    <!-- Retrieve data from db -->
<?php
$server="localhost";
$usr="root";
$pwd="";
$db="eventino_db";

$eventi_trovati = 1;
$artisti_trovati = 1;
if (isset($_GET)){
    $mysqli = new mysqli($server,$usr,$pwd,$db);

    if ($mysqli -> connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
        exit();
    }
    //l'input puo' esere sia un evento che un artista
    $input = htmlentities($_GET['search'], ENT_QUOTES);
    $input = strtolower($input);
    $input = '%'.$input.'%';
///ricerco prima negli eventi
    if ($stmt = $mysqli->prepare("SELECT Codice_evento, Img_evento, Nome_evento, Descrizione FROM evento WHERE lower(Nome_evento) like ? LIMIT 10")) { 
        $stmt->bind_param('s', $input); // esegue il bind del parametro '$email' e $username.
        $stmt->execute(); // esegue la query appena creata.
        $rows = $stmt->get_result();
        if($rows->num_rows===0) {
            $eventi_trovati = 0;
        }
        while($row = $rows->fetch_assoc()){
            $nome_evento=$row["Nome_evento"];
            $img=$row["Img_evento"];
            $desc=$row["Descrizione"];
            $codice_evento=$row["Codice_evento"];
            $sql = "SELECT Importo FROM `prodotto` WHERE FK_Codice_evento='$codice_evento' ORDER BY Importo asc LIMIT 1"; 
            if ($result2 = $mysqli->query($sql)) {
                $biglietto = $result2->fetch_assoc();
                $prezzo = $biglietto["Importo"];
            } else {
                $prezzo = "err";
            }

            echo "<div class='card'>
                  <a href='event-page.php?event=$codice_evento' class='card-link'>
                    <img class='carousel-img card-img-top img-fluid' src='$img' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>
                  </a>
                  <div class='card-body'>
                    <h5 class='card-title'> $nome_evento </h5>
                    <p class='card-text'> $desc </p>
                    <a href='event-page.php?event=$codice_evento' class='card-link'>
                      <button type='button' class='btn btn-secondary'>A partire da $prezzo $</button>
                    </a>
                  </div>
                </div>";
          }
        }

////ora ricerco gli artisti
    if ($stmt = $mysqli->prepare("SELECT Nome_ospite, Img_ospite, FK_Nome_categoria_ospite, Descrizione FROM ospite WHERE lower(Nome_ospite) like ? ")) { 
        $stmt->bind_param('s', $input); // esegue il bind del parametro '$email' e $username.
        $stmt->execute(); // esegue la query appena creata.
        $rows = $stmt->get_result();
        if($rows->num_rows===0) {
            $artisti_trovati = 0;
        }
        //lo metto qui almeno lo faccio una volta sola
        if(login_check($mysqli)){
          $usr=$_SESSION["username"];
        }
        while($row = $rows->fetch_assoc()){
            $nome_ospite=$row["Nome_ospite"];
            $img=$row["Img_ospite"];
            $nome_categoria=$row["FK_Nome_categoria_ospite"];
            $desc=$row["Descrizione"];
            if (login_check($mysqli)){//se sono loggato personalizzo un minimo, altrimenti nulla
              $sql = "SELECT * FROM segue WHERE FK_Nome_ospite='$nome_ospite' AND FK_UserName = '$usr'";
            }else{
              $sql = "SELECT * FROM segue WHERE FK_Nome_ospite='$nome_ospite'";
            }
            $seguito = $mysqli->query($sql);
            if ($seguito->num_rows>0 && login_check($mysqli)) {
                echo"<div class='card'>
                    <a href='guest_page.php?guest=$nome_ospite' class='card-link'>
                      <img class='carousel-img card-img-top img-fluid' src='$img' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>
                    </a>
                  <div class='card-body'>
                    <h5 class='card-title'> $nome_ospite </h5>
                    <p class='card-text'> $desc </p>
                    <button type='button' class='btn btn-secondary' onclick='setFollow(`$nome_ospite`,`$usr`)' id='follow_btn'>Segui già</button>
                  </div>
                </div>";
            } else{
                echo"<div class='card'>
                      <a href='guest_page.php?guest=$nome_ospite' class='card-link'>
                        <img class='carousel-img card-img-top img-fluid' src='$img' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>
                      </a>
                  <div class='card-body'>
                    <h5 class='card-title'> $nome_ospite </h5>
                    <p class='card-text'> $desc </p>";
                    if(login_check($mysqli)){
                      echo"<button type='button' class='btn btn-secondary' onclick='setFollow(`$nome_ospite`,`$usr`)' id='follow_btn'>Segui</button>
                      </div>
                    </div>";
                    }else{
                      echo"<a href='login.php'> <button type='button' class='btn btn-secondary'>Segui</button></a>
                      </div>
                    </div>";
                    }
                    
            }
            //<a href='event-page.php?event=$codice_evento' class='card-link'></a>

            //manca il link all'artista
            
      }
    }
    $mysqli->close();
}

    if ($eventi_trovati == 0 && $artisti_trovati == 0){
        echo"<div>
                <p>Non sono stati trovati risultati</p>
            </div>";
    }

    // Before-Footer links
      echo "<br/><a class='before-footer' href='events.php'> Visualizza tutti gli eventi </a>";

    // Footer 
      require_once("footer.html");
    ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>

</html>
