<?php
  include 'functions.php';
  sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Jquery Libraries -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

    <!-- Ajax lib 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    -->
    
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" 
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" 
    crossorigin="anonymous">

    <!-- Jquery-Ui -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Main Css -->
    <link rel="stylesheet" href="css/main.css">

    <script src="control.js"></script>
    <script src="verify.js"></script>
    <title> Nuovo evento | Event.ino </title>

  </head>

  <body>
    <?php
    
        // Header    
        require_once("header.php");   

        //Db connection
        require_once("connection.php");
    ?>

    <h3 class="title">Aggiungi evento</h3>

    <form action="/adding-event.php" class="needs-validation" novalidate>
        
        <div class="form-row">
            <!--Insert nome-->
            <div class="col">
                <label for="event_name">Nome evento:</label>
                <input type="text" class="form-control" id="event_name" placeholder="Nome evento" name="event_name" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci il nome dell'evento</div>
            </div>
            <!--Insert immagine-->
            <div class="col">
                <label for="event_image">Percorso locandina:</label>
                <input type="text" class="form-control" id="event_image" placeholder="Percorso immagine" name="event_image" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci immagine</div>
            </div>
        </div>
        <!--Insert descrizione ( text area ) -->
        <div class="form-group">
            <label class="label-text-area" for="descrizione_evento">Descrizione manifestazione</label>
            <textarea class="form-control" id="descrizione_evento" rows="5" 
            name="event_desc" placeholder="Inserisci descrizione evento" required></textarea>
            <div class="valid-feedback"> Valido </div>
            <div class="invalid-feedback">Perfavore, inserisci la descrizione</div>
        </div>
        <div class="form-row">
            <!--Insert posti-->
            <div class="col">
                <label for="posti_evento">Posti disponibili:</label>
                <input type="number" class="form-control" id="posti_evento" placeholder="0" name="posti_evento" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci numero posti</div>
            </div>
            <!--Insert biglietti-->
            <div class="col">
                <label for="biglietti_evento">Biglietti venduti:</label>
                <input type="number" class="form-control" id="biglietti_evento" placeholder="0" 
                name="biglietti_evento" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci biglietti venduti</div>
            </div>
        </div>
        <div class="form-row">
            <!-- Selezione tipologia evento --> 
            <div class="col">
                <label for="form-selected-tipo">Tipologia evento</label>
                <select class="form-control" id="form-selected-tipo" name="categoria_evento"> 
                <?php
                    $sql = "SELECT Nome_categoria_evento FROM categoria_evento";
                    $result = $conn->query($sql);    
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { echo '<option>' . $row['Nome_categoria_evento'] . '</option>';}
                    } else {
                        echo "0 results";
                    } 
                ?>
                </select>
            </div>
            <!-- Selezione struttura --> 
            <div class="col">
                <label for="form-selected-struttura"> Struttura </label>
                <select class="form-control" id="form-selected-struttura" name="nome_struttura"> 
                <?php
                    $sql = "SELECT `Nome_struttura` FROM `struttura`";
                    $result = $conn->query($sql);    
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { echo '<option>' . $row['Nome_struttura'] . '</option>';}
                    } else {
                        echo "0 results";
                    }  
                ?>
                </select>
            </div>
        </div>
        <div class="form-row">
        <!-- Selezione manifestazione --> 
            <div class="col">
                <label for="form-selected-manifestazione"> Nome manifestazione </label>
                <select class="form-control" id="form-selected-manifestazione" name="nome_manifestazione"> 
                <?php
                    $sql = "SELECT Nome_manifestazione FROM manifestazione";
                    $result = $conn->query($sql);    
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) { echo '<option>' . $row['Nome_manifestazione'] . '</option>';}
                    } else {
                        echo "0 results";
                    }  
                ?>
                </select>
            </div>
            <!--Insert num.ospiti-->
            <div class="col">
                <label for="num_ospiti">Numero ospiti:</label>
                <input type="number" class="form-control" id="num_ospiti" placeholder="0" 
                name="num_ospiti" required>
                <div class="valid-feedback">Valido</div>
                <div class="invalid-feedback">Inserisci numero ospiti</div>
            </div>
        </div>
        
        <!-- Scelta data -->
        <div class="form-group">
                <label for="datepicker">Data evento:</label>
                <input type="text" name="data_evento" class="datepicker" id="datepicker" required/>
        </div>

        <!-- Submit -->
        <div class="form-group">
                    <label id="disable" for="disable_button"></label>
                    <button id="disable_button" formaction="adding-event.php" 
                    formmethod="POST" type="submit" class="btn btn-primary mb-2">Inserisci</button>
        </div>
</form>
        <?php
            echo "<a href='user_profile.php'>
            <button type='button' class='btn btn-primary'>FINISH</button>
          </a>";
        ?>
<script> // Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<?php  
    require_once("footer.html"); 
    $conn->close();
?>  

 <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body> 

</html>