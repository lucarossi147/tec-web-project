<?php
include 'functions.php';
sec_session_start();

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once("connection.php");

if (isset($_POST)){

    $nome = htmlentities($_POST["name-man"],ENT_QUOTES);
    $num_eventi = htmlentities($_POST["num-man"],ENT_QUOTES);
    $immagine = htmlentities($_POST["image-man"],ENT_QUOTES);
    $descrizione = htmlentities($_POST["desc-man"],ENT_QUOTES);
    $data_inizio = htmlentities($_POST["datepickerstart"],ENT_QUOTES);
    $data_fine = htmlentities($_POST["datepickerfinish"],ENT_QUOTES);
    
    //Controllo correttezza data
    if($data_fine < $data_inizio){
            echo "<p>Hai inserito una data finale antecedente a quella iniziale</p>";
            echo "<a href='insert_manifest.php'>Torna</p>"; 
            exit();
    }

    //Recupero username utente loggato
    $user = $_SESSION['username'];

    if($stmt= $conn->prepare("SELECT Organizzatore,Amministratore FROM utente WHERE UserName=?")){

        //Bind 
        $stmt->bind_param("s",$user);

        //Execute
        $stmt->execute();

        //Ottieni result
        $result = $stmt->get_result();

        //Estraggo risultato -> organizz. e amministratore
        $row = $result->fetch_array(MYSQLI_NUM);
        $org = $row[0];
        $amm = $row[1];

        if($org == 0 && $amm == 0){
            $error = $conn->errno . ' ' . $conn->error;
            echo $error;
            echo "<p>Errore diritti amministratore</p>";
            echo "<a href='insert_manifest.php'>Torna</p>"; 
            exit();
        }
    }
            
    //Creo statement per inserire una nuova manifestazione e controllo        
    if($stmt= $conn->prepare("INSERT INTO `manifestazione` (`Codice_manifestazione`, `FK_UserName`,
    `Data_inizio`, `Data_fine`, `Num_eventi`, `Nome_manifestazione`, `Descrizione`, `Img_manifestazione`) 
    VALUES (NULL,?,?,?,?,?,?,?)")){

        //Bind dei parametri
        $stmt->bind_param("sssssss",$user, $data_inizio, $data_fine, $num_eventi, $nome, $descrizione, $immagine);
            
        //Eseguo query
        $stmt->execute();

        //Ottengo risultato
        $result = $stmt->get_result();
        console_log($result);
            
            if($stmt= $conn->prepare("SELECT Mail FROM utente WHERE UserName=?")){
                //Bind 
                $stmt->bind_param("s",$user);

                //Execute
                $stmt->execute();

                //Ottieni result
                $result = $stmt->get_result();

                //Row contiene l'unico risultato possibile essendo user una chiave
                $row = $result->fetch_array(MYSQLI_NUM);
                $mail = $row[0];

                //Creo messagggio
                $message = "La manifestazione è stata aggiunta";
                $message = wordwrap($message, 70, "\r\n");

                //Invio mail (mail, object of mail, message)
                //TODO
            sendmail($mail, 'Aggiunta manifestazione', $message);
            }
            else {  //Errore select mail
                    //Se avviene un errore, stampo il codice e stampo link per redirect
                    $error = $conn->errno . ' ' . $conn->error;
                    echo $error;
                    echo "<p>Errore nello script, torna alla pagina di creazione</p>";
                    echo "<a href='insert_manifest.php'>Torna</p>";
            }
        }else { //Errore insert
            //Se avviene un errore, stampo il codice e stampo link per redirect
            $error = $conn->errno . ' ' . $conn->error;
            echo $error;
            echo "<p>Errore nello script, torna alla pagina di creazione</p>";
            echo "<a href='insert_manifest.php'>Torna</p>";
            exit();
        }
        //console_log("Manifestazione inserita");
        //Redirect
}
?>