<?php
    include 'functions.php';
    sec_session_start();
?>
<!doctype html>
<html lang="it">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>

    <!-- Main Css -->
    <link rel="stylesheet" href="css/main.css">
    <title>Artista | Event.ino</title>
  </head>

  <body>

    <!-- 
        Header 
    -->
    <?php
      require_once("header.php");
    ?>

    <!-- Retrieve data from db -->
    <?php

    require_once("connection.php");

    if(isset($_GET["guest"])){
      $guest=$_GET["guest"];
    }

    if($stmt = $conn->prepare("SELECT * FROM `ospite` WHERE Nome_ospite=?")){
      $stmt->bind_param('s',$guest);
      $stmt->execute();
      $result = $stmt->get_result();
      $stmt->close();

      $img='';
      $desc='';
      if ($result->num_rows > 0) {
          // output data of each row, usually only one row
          while($row = $result->fetch_assoc()) {
              $desc=$row["Descrizione"];
              $img=$row["Img_ospite"];
          }
          echo "
          <div class='row'>
              <h1> $guest</h1>
          </div>
          <div class='row'>
            <div class='col'>
                <div class='row'>
                  <img class='carousel-img card-img-top img-fluid' src='$img' onerror=\"this.onerror=null;this.src='images/grey.png'\" alt='missing'>
                </div>
              </div>";
      } else {
          echo "0 results";
      }
    }
 
    if($stmt = $conn->prepare("SELECT * FROM `evento` 
    WHERE Codice_evento IN (SELECT FK_Codice_evento
    FROM `partecipazione` WHERE FK_Nome_ospite=?)")){
      $stmt->bind_param('s', $guest);
      $stmt->execute();
      $result = $stmt->get_result();
      $stmt->close();
      echo "<div class='container'>";            
      if($result->num_rows>0){
        while($evento = $result->fetch_assoc()){
          $name = $evento["Nome_evento"];
          $event_id = $evento["Codice_evento"];
          $stmt = $conn->prepare("SELECT Importo FROM `prodotto` WHERE FK_Codice_evento=? ORDER BY Importo asc LIMIT 1");
          $stmt->bind_param('i', $event_id);
          $stmt->execute();
          $result = $stmt->get_result();
          $stmt->close();
          $price=NULL;
          if (isset($result)) {
              $product = $result->fetch_assoc();
              $price=$product["Importo"];
          } else {
              $price = "err";
          }
          echo "<div class='row'>
                    <div class='col-8'> $name</div>
                    <div class='col-4'>
                      <a href='event-page.php?event=$event_id' class='card-link'>
                          <button type='button' class='btn btn-secondary'>A partire da $price $</button>
                      </a>
                    </div>";
          }
                    
          echo "</div>";
      } 
    }
    echo"</div>";
    echo"         </div>
              </div>
              <div class='col'>
                  <p> $desc</p>
              </div>
            </div>
          </div>";

    require_once("footer.html");
    
    ?>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>

</html>